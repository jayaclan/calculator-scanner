package com.calculator.base.network.refreshtoken

data class RefreshTokenResponse(
    val message: String,
    val statusCode: Int,
    val data: TokenData
)
package com.calculator.base.network.refreshtoken

import com.google.gson.annotations.SerializedName

data class Token(
    @SerializedName("token")
    val token: String
)
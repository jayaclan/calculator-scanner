package com.calculator.base.exception

class SessionExpiredException(exMessage: String?) : Exception(exMessage)
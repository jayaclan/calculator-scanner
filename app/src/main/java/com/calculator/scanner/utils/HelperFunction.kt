package com.calculator.scanner.utils

import java.text.DateFormatSymbols

class HelperFunction {
    fun convertToMonthName (monthNumber: Int) : String {
        return DateFormatSymbols().months[monthNumber]
    }
}
package com.calculator.scanner.utils

import java.util.*

private const val SECOND_MILLIS = 1000
private const val MINUTE_MILLIS = 60 * SECOND_MILLIS
const val HOUR_MILLIS = 60 * MINUTE_MILLIS
private const val DAY_MILLIS = 24 * HOUR_MILLIS

private fun currentDate(): Date {
    val calendar = Calendar.getInstance()
    return calendar.time
}

fun getTimeAgo(date: Date): String {
    var time = date.time
    if (time < 1000000000000L) {
        time *= 1000
    }

    val now = currentDate().time
    if (time > now || time <= 0) {
        return "in the future"
    }

    val diff = now - time
    return when {
        diff < MINUTE_MILLIS -> "Beberapa saat lalu"
        diff < 2 * MINUTE_MILLIS -> "Beberapa menit lalu"
        diff < 60 * MINUTE_MILLIS -> "${diff / MINUTE_MILLIS} menit lalu"
//        diff < 2 * HOUR_MILLIS -> "di bawah 1 jam"
        diff < 24 * HOUR_MILLIS -> "${diff / HOUR_MILLIS} Jam lalu"
        diff < 48 * HOUR_MILLIS -> "Kemarin"
        else -> "${diff / DAY_MILLIS} Hari lalu"
    }
}
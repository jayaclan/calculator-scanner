package com.calculator.scanner.presentation.uploadImage

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.calculator.scanner.presentation.uploadImage.component.imageRecognition
import com.calculator.scanner.ui.components.button.BVTButton
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.*


@Composable
@Preview(showBackground = true)
fun UploadImageScreen(
    viewModel: UploadImageViewModel = hiltViewModel(),
    onClickBack: () -> Unit = {},
    onClickScan: () -> Unit = {},
    onClickSetting: () -> Unit = {},
){

    val uiState = viewModel.uiState
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(if (uiState.changeTheme) BlueOnesan2 else RedOneSan)
            .statusBarsPadding()
            .background(Platinum),
    ) {
        imageRecognition(
            viewModel = viewModel,
            onClickBack = onClickBack
        )

        BVTText(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .background(Color.White),
            text = uiState.extractedText ?: "",
            color = TextBlack,
            style = BVTTypography.H3Regular,
            maxLines = 100,
        )


        Column(modifier = Modifier.fillMaxWidth()) {


            if (uiState.isSuccessScan)
                BVTText(
                    modifier = Modifier
                        .fillMaxWidth(),
                    text = "Founded ${uiState.successText?.input}",
                    color = DusRed6,
                    style = BVTTypography.H3Regular,
                    maxLines = 1,
                )

            BVTButton(
                modifier = Modifier
                    .padding(horizontal = 4.dp)
                    .fillMaxWidth(),
                text = "Succes Scan",
                enabled = uiState.isSuccessScan,
                type = if (uiState.changeTheme) BVTButton.BlueOneSan2 else BVTButton.RedOneSan2,
                textAlign = TextAlign.Center,
                onClick = {
                    onClickScan()
                }
            )
        }
    }
}
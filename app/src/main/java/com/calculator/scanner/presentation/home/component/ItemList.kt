package com.calculator.scanner.presentation.home.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.calculator.scanner.R
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.BVTTypography
import com.calculator.scanner.ui.theme.BlueOnesan2
import com.calculator.scanner.ui.theme.GrayOneSan
import com.calculator.scanner.ui.theme.Platinum


@Composable
@Preview(showBackground = true)
fun ItemList(
    input: String = "",
    result: String = "",
    onClickRemove: () -> Unit = {},
    color: Color = BlueOnesan2,
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 8.dp),
        backgroundColor = GrayOneSan,
        shape = RoundedCornerShape(10.dp),
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(
                modifier = Modifier
                    .weight(1f)
                    .padding(8.dp)
            ) {
                BVTText(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 4.dp),
                    text = "Input : $input",
                    color = Platinum,
                    style = BVTTypography.H3Regular,
                    maxLines = 1,
                )

                BVTText(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 4.dp),
                    text = "Result : $result",
                    color = BlueOnesan2,
                    style = BVTTypography.H3Regular,
                    maxLines = 1,
                )
            }


            Icon(
                modifier = Modifier
                    .clickable {
                        onClickRemove()
                    }
                    .size(36.dp),
                painter = painterResource(id = R.drawable.ic_baseline_delete_24),
                contentDescription = null,
                tint = color
            )

        }

    }
}
package com.calculator.scanner.presentation.home.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.calculator.scanner.presentation.ConstantManager.Companion.INPUT
import com.calculator.scanner.presentation.ConstantManager.Companion.ITEM_RESULT_LIST
import com.calculator.scanner.presentation.ConstantManager.Companion.RESULT
import kotlinx.parcelize.Parcelize


@Entity(tableName = ITEM_RESULT_LIST)
@Parcelize
data class ItemResult(
    @PrimaryKey(autoGenerate = true)
    var itemId: Long = 0L,

    @ColumnInfo(name = INPUT)
    val input: String? = null,

    @ColumnInfo(name = RESULT)
    val result: String? = null,
) : Parcelable
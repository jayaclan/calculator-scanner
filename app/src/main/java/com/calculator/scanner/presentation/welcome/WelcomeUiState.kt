package com.calculator.scanner.presentation.welcome

data class WelcomeUiState(
    val isLoading: Boolean = false,
    val intro: List<String> = listOf("Follow IG: @onechanofficial",
        "Join Grup Telegram : @Onechanofficial",
        "liat update compose terbaru di grup telegram",
        "Request compose kesayangan kalian",
        "Boruto adalah anaknya Naruto...",
        "Terimah kasih pelanggan setia One Chan",
        "Push up 100x, lari 10 km, biar kuat seperti saitama",
        "yakali gak maraton compose",
        "Happy nonton compose gaes",
        "selalu cek versi terbaru di PlayStore"),
    val showIntro: String? = null,
)
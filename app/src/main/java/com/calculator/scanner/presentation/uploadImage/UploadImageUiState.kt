package com.calculator.scanner.presentation.uploadImage

import android.graphics.Bitmap
import com.calculator.scanner.presentation.home.model.ItemResult

data class UploadImageUiState(
    val isLoading: Boolean = false,
    val extractedText: String? = null,
    val successText: ItemResult? = null,
    val isSuccessScan: Boolean = false,

    val image: Bitmap? = null,
    val changeTheme:Boolean = false,
)
package com.calculator.scanner.presentation.camera

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.Surface
import com.calculator.base.activity.BaseActivity
import com.calculator.scanner.ui.theme.BVTComposeTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CameraActivity : BaseActivity() {
    companion object {
        const val INTENT_ITEM = "INTENT_ITEM"
    }

    private val viewModel: CameraViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadTheme()
        setContent {
            BVTComposeTheme {
                Surface {
                    CameraScreen(
                        viewModel = viewModel,
                        onClickBack = {
                            onBackPressed()
                        },
                        onClickSetting = {
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri: Uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivity(intent)
                        },
                        onClickScan = {
                            val intent = Intent().apply {
                                putExtra(INTENT_ITEM,
                                    viewModel.uiState.successText)
                            }
                            setResult(RESULT_OK, intent)
                            finish()
                        }
                    )
                }
            }
        }
        setupObserver()
    }


    override fun setupObserver() {

    }

}
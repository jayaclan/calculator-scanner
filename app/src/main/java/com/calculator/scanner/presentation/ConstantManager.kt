package com.calculator.scanner.presentation

class ConstantManager {
    companion object {


        const val SHARED_PREFERENCES = "SHARED_PREFERENCES"

        const val changeTheme = "changeTheme"

        const val ITEM_RESULT_LIST = "item_result_list"

        const val INPUT = "input"
        const val RESULT = "result"
    }
}
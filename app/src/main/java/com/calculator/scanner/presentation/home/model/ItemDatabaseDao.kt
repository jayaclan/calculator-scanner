package com.calculator.scanner.presentation.home.model

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface ItemDatabaseDao {

    @Query("SELECT * FROM ITEM_RESULT_LIST")
    fun getAll(): List<ItemResult>

    @Query("SELECT * from ITEM_RESULT_LIST where itemId = :id")
    fun getById(id: Int): ItemResult?

    @Insert
    suspend fun addItem(item: ItemResult)

    @Update
    suspend fun update(item: ItemResult)

    @Delete
    suspend fun delete(item: ItemResult)

    @Query("DELETE FROM ITEM_RESULT_LIST")
    suspend fun deleteAllTodos()
}

package com.calculator.scanner.presentation.home

import com.calculator.scanner.presentation.home.model.ItemResult

data class HomeUiState(
    val isLoading: Boolean = false,
    val isDialogChoosePhoto: Boolean = false,

    val result: List<ItemResult> = listOf(),
    val changeTheme:Boolean = false,
)

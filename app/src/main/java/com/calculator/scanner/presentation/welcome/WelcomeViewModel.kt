package com.calculator.scanner.presentation.welcome

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.calculator.base.viewmodel.BaseViewModel
import com.calculator.domain.usecase.device.DeviceUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class WelcomeViewModel @Inject constructor(
//    private val authUseCase: AuthUseCase,
    private val deviceUseCase: DeviceUseCase,
) : BaseViewModel() {

    var uiState by mutableStateOf(WelcomeUiState())
        private set


//    fun getDataListAnime() {
//        uiState = uiState.copy(isLoading = true)
//        val db = Firebase.firestore
//        db.collection("listanime")
//            .orderBy("year", Query.Direction.DESCENDING)
////            .startAfter("BlueLockS1")
////            .limit(5)
//            .get()
//            .addOnSuccessListener { documents ->
//                val listAnime = documents.toObjects<EntityVideo>()
//                successGetDataListAnime.postValue(listAnime)
//                uiState = uiState.copy(isLoading = false)
//            }
//            .addOnFailureListener { exception ->
//                failedGetDataListAnime.postValue(exception.toString())
//                uiState = uiState.copy(isLoading = false)
//            }
//    }

    fun showIntroString() {
        val rnds = (0 until uiState.intro.size).random()
        val showIntro = uiState.intro[rnds]

        uiState = uiState.copy(showIntro = showIntro)
    }


}
package com.calculator.scanner.presentation.welcome

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.Surface
import androidx.lifecycle.lifecycleScope
import com.calculator.base.activity.BaseActivity
import com.calculator.scanner.presentation.home.HomeActivity
import com.calculator.scanner.ui.theme.BVTComposeTheme
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.reflect.KClass

@AndroidEntryPoint
class WelcomeActivity : BaseActivity() {

    private val viewModel: WelcomeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            viewModel.showIntroString()
        }
//        FirebaseMessaging.getInstance().subscribeToTopic("all");
        setContent {
            BVTComposeTheme {
                Surface {
                    WelcomeScreen(viewModel = viewModel)
                }
            }
        }
        setupObserver()
    }


    private fun firebaseMessagings() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                print(" asd12312323 gagal")
                return@OnCompleteListener
            }
            // Get new FCM registration token
            val token = task.result
            print("$token asd12312323")

        })
    }

    override fun setupObserver() {
        launch {
            delay(2222L)
            intentTo(
                HomeActivity::class
            )
            finish()
        }
    }

    private fun intentTo(activity: KClass<out BaseActivity>) {
        val intent = Intent(this, activity.java)
        startActivity(intent)
    }

}
package com.calculator.scanner.presentation.uploadImage

import android.content.SharedPreferences
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.viewModelScope
import com.calculator.base.viewmodel.BaseViewModel
import com.calculator.scanner.presentation.ConstantManager
import com.calculator.scanner.presentation.home.model.ItemResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UploadImageViewModel @Inject constructor(
    private val sharedPreferences: SharedPreferences
) : BaseViewModel() {
    var uiState by mutableStateOf(UploadImageUiState())
        private set


    fun setState(uiState: UploadImageUiState) {
        viewModelScope.launch {
            var successText = ItemResult()
            var isSuccessScan = false


            if (uiState.extractedText != null) {
                var getValue = false
                var extractedText = uiState.extractedText
                do{
                    val expression = extractedText?.indexOfFirst {
                        it.equals('+')
                                || it.equals('x',
                            ignoreCase = true)
                                || it.equals('/')
                                || it.equals('-')
                                || it.equals('*')
                                || it.equals(':')
                    }

                    if (extractedText?.isNotEmpty() == true
                        && expression != extractedText.length - 1
                        && (expression ?: 0) > 0
                    ) {
                        if (extractedText[(expression ?: 0) + 1].isDigit()
                            && extractedText[(expression ?: 0) - 1].isDigit()
                        ) {
                            getValue = true
                            val firstDigit =
                                extractedText.get((expression ?: 0) - 1).digitToIntOrNull()
                            val secondDigit =
                                extractedText.get((expression ?: 0) + 1).digitToIntOrNull()

                            if (extractedText[(expression ?: 0)] == '+') {
                                successText = ItemResult(
                                    input = "$firstDigit + $secondDigit",
                                    result = "${(firstDigit ?: 0) + (secondDigit ?: 0)}"
                                )
                                isSuccessScan = true
                            }


                            if (extractedText[(expression ?: 0)].equals('x',
                                    ignoreCase = true) || extractedText[(expression
                                    ?: 0)].equals(
                                    '*',
                                    ignoreCase = true)
                            ) {
                                successText = ItemResult(
                                    input = "$firstDigit x $secondDigit",
                                    result = "${(firstDigit ?: 0) * (secondDigit ?: 0)}"
                                )
                                isSuccessScan = true
                            }

                            if (extractedText[(expression ?: 0)].equals('-',
                                    ignoreCase = true)
                            ) {
                                successText = ItemResult(
                                    input = "$firstDigit - $secondDigit",
                                    result = "${(firstDigit ?: 0) - (secondDigit ?: 0)}"
                                )
                                isSuccessScan = true
                            }

                            if (extractedText[(expression ?: 0)].equals('/',
                                    ignoreCase = true) || extractedText[(expression
                                    ?: 0)].equals(
                                    ':',
                                    ignoreCase = true)
                            ) {
                                successText = ItemResult(
                                    input = "$firstDigit : $secondDigit",
                                    result = "${(firstDigit ?: 0) / (secondDigit ?: 0)}"
                                )
                                isSuccessScan = true

                            }
                        } else {
                            extractedText = extractedText.removeRange(0, (expression ?: 0) + 1)
                        }
                    } else {
                        successText = ItemResult()
                    }
                }while (expression != -1 && !getValue)
            }


            val newState = uiState.copy(
                successText = successText,
                isSuccessScan = isSuccessScan
            )

            this@UploadImageViewModel.uiState = newState
        }
    }

    fun loadTheme() {
        val changeTheme = sharedPreferences.getBoolean(ConstantManager.changeTheme, false)
        uiState = uiState.copy(changeTheme = changeTheme)

    }
}
package com.calculator.scanner.presentation.home

import android.Manifest
import android.app.Activity
import android.content.Intent
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Scaffold
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.calculator.scanner.presentation.camera.CameraActivity
import com.calculator.scanner.presentation.home.component.ItemList
import com.calculator.scanner.presentation.home.model.ItemResult
import com.calculator.scanner.presentation.uploadImage.UploadImageActivity
import com.calculator.scanner.ui.components.button.BVTButton
import com.calculator.scanner.ui.components.dialog.BVTChoosePhoto
import com.calculator.scanner.ui.components.topbar.TopBarIconic
import com.calculator.scanner.ui.theme.BlueOnesan2
import com.calculator.scanner.ui.theme.Platinum
import com.calculator.scanner.ui.theme.RedOneSan
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable
@Preview(showBackground = true)
private fun Preview() {
    HomeScreen()
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
internal fun HomeScreen(
    viewModel: HomeViewModel = hiltViewModel(),
) {
    val uiState = viewModel.uiState
    val localDensity = LocalDensity.current
    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()
    val scaffoldState = rememberScaffoldState()

    val storagePermissionState = rememberPermissionState(Manifest.permission.READ_EXTERNAL_STORAGE)


    val intentWithResult =
        rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                it.data?.getParcelableExtra<ItemResult>(CameraActivity.INTENT_ITEM)?.let { item ->
                    val result = uiState.result.toMutableList()
                    result.add(item)
                    viewModel.setState(
                        uiState.copy(
                            result = result,
                            isDialogChoosePhoto = false
                        )
                    )

                    coroutineScope.launch(Dispatchers.IO) {
                        viewModel.saveData(item)
                    }
                }
            }

        }


    Scaffold(
        modifier = Modifier
            .fillMaxSize()
            .background(if (uiState.changeTheme) BlueOnesan2 else RedOneSan)
            .statusBarsPadding()
            .background(Platinum),
        scaffoldState = scaffoldState,
        floatingActionButton = {
            BVTButton(
                modifier = Modifier
                    .padding(horizontal = 4.dp),
                text = "Add Input",
                type = if (uiState.changeTheme) BVTButton.BlueOneSan2 else BVTButton.RedOneSan2,
                textAlign = TextAlign.Center,
                onClick = {
                    viewModel.setState(uiState.copy(isDialogChoosePhoto = true))
                }
            )
        }
    ) { scaffold ->
        Column(
            Modifier
                .fillMaxSize()
                .padding(bottom = scaffold.calculateBottomPadding())
        ) {
            TopBarIconic(
                darkTheme = uiState.changeTheme,
                onClickTheme = {
                    viewModel.setState(uiState.copy(changeTheme = !uiState.changeTheme))
                }
            )

            LazyColumn(
                modifier = Modifier
                    .weight(1f)
            ) {
                items(items = viewModel.uiState.result) {
                    ItemList(
                        input = it.input ?: "",
                        result = it.result ?: "",
                        onClickRemove = {
                            coroutineScope.launch (Dispatchers.IO){
                                viewModel.deleteItem(it)
                            }
                        },
                        color = if (uiState.changeTheme) BlueOnesan2 else RedOneSan
                    )
                }

            }

        }
    }


    BVTChoosePhoto(
        statusShow = uiState.isDialogChoosePhoto,
        onClickTakePhoto = {
            val intent = Intent(context, CameraActivity::class.java)
            intentWithResult.launch(intent)
        },
        onClickChooseGallery = {
            val intent = Intent(context, UploadImageActivity::class.java)
            intentWithResult.launch(intent)
        },
        onDismiss = {
            viewModel.setState(uiState.copy(isDialogChoosePhoto = false))
        }
    )
}



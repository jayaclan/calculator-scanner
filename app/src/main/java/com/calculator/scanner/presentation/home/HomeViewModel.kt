package com.calculator.scanner.presentation.home

import android.content.SharedPreferences
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.viewModelScope
import com.calculator.base.viewmodel.BaseViewModel
import com.calculator.scanner.presentation.ConstantManager.Companion.changeTheme
import com.calculator.scanner.presentation.home.model.ItemRepository
import com.calculator.scanner.presentation.home.model.ItemResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject


@HiltViewModel
class HomeViewModel @Inject constructor(
    private val sharedPreferences: SharedPreferences,
    private val repository: ItemRepository
) : BaseViewModel() {

    var uiState by mutableStateOf(HomeUiState())
        private set


    private fun currentDate(): Date {
        val calendar = Calendar.getInstance()
        return calendar.time
    }

    fun setState(uiState: HomeUiState) {
        viewModelScope.launch {
            sharedPreferences.edit().putBoolean(changeTheme, uiState.changeTheme).apply()
            this@HomeViewModel.uiState = uiState
        }
    }

    fun getData(){
        val result = repository.getAll()
        uiState  = uiState.copy(
            result = result
        )
    }


    suspend fun saveData(itemResult: ItemResult){
        repository.addItem(itemResult)
    }

    suspend fun deleteItem(itemResult: ItemResult){
        repository.deleteItem(itemResult)
        getData()
    }
}

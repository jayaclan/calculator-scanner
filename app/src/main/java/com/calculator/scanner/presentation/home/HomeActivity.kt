package com.calculator.scanner.presentation.home

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.Surface
import androidx.lifecycle.lifecycleScope
import com.calculator.base.activity.BaseActivity
import com.calculator.scanner.ui.theme.BVTComposeTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class HomeActivity : BaseActivity() {
    companion object {

    }

    private val viewModel: HomeViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launch {
            viewModel.getData()
        }


        setContent {
            BVTComposeTheme(
                isDarkTheme = viewModel.uiState.changeTheme
            ) {
                Surface {
                    HomeScreen(
                        viewModel = viewModel,
                    )
                }
            }
        }
        setupObserver()
    }


    override fun setupObserver() {

    }
}
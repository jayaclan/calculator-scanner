package com.calculator.scanner.presentation.uploadImage.component

import android.annotation.SuppressLint
import android.graphics.ImageDecoder
import android.os.Build
import android.provider.MediaStore
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.rememberAsyncImagePainter
import com.calculator.scanner.presentation.uploadImage.UploadImageViewModel
import com.calculator.scanner.ui.components.button.BVTButton
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.TextRecognition
import java.io.IOException
import java.util.concurrent.Executors


@SuppressLint("UnsafeOptInUsageError")
@Composable
fun imageRecognition(
    onClickBack: () -> Unit = {},
    viewModel: UploadImageViewModel = hiltViewModel(),
) {

    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current
    val cameraProviderFuture = remember { ProcessCameraProvider.getInstance(context) }
    var preview by remember { mutableStateOf<Preview?>(null) }
    val executor = ContextCompat.getMainExecutor(context)
    val cameraProvider = cameraProviderFuture.get()
    val textRecognizer = remember { TextRecognition.getClient() }
    val cameraExecutor = remember { Executors.newSingleThreadExecutor() }

    val uiState = viewModel.uiState


    val launcherGallery =
        rememberLauncherForActivityResult(ActivityResultContracts.OpenDocument()) {
            it?.let { uri ->
                val bitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    ImageDecoder.decodeBitmap(
                        ImageDecoder.createSource(
                            context.contentResolver,
                            uri
                        )
                    )
                } else {
                    MediaStore.Images.Media.getBitmap(context.contentResolver, uri)
                }

                viewModel.setState(uiState.copy(image = bitmap))
                try {
                    val image = InputImage.fromFilePath(context, uri)
                    textRecognizer.process(image)
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                viewModel.setState(
                                    viewModel.uiState.copy(
                                        extractedText = it.result?.text ?: ""
                                    )
                                )
                            }
                        }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }

    Box {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.7f),
            verticalArrangement = if (uiState.image != null) Arrangement.Top else Arrangement.Center
        ) {
            if (uiState.image != null)
                Image(
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f),
                    painter = rememberAsyncImagePainter(uiState.image),
                    contentDescription = null,
                )
            BVTButton(
                modifier = Modifier
                    .padding(horizontal = 4.dp)
                    .fillMaxWidth(),
                text = "Upload Photo",
                type = BVTButton.Primary,
                textAlign = TextAlign.Center,
                onClick = {
                    launcherGallery.launch(arrayOf("image/jpeg", "image/png"))
                }
            )
        }

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(15.dp)
                .align(Alignment.TopStart)
        ) {
            IconButton(
                onClick = {
                    onClickBack()
                }
            ) {
                Icon(
                    imageVector = Icons.Filled.ArrowBack,
                    contentDescription = "back",
                    tint = Color.White
                )
            }
        }
    }
}
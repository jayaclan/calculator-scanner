package com.calculator.scanner.presentation.camera

import com.calculator.scanner.presentation.home.model.ItemResult

data class CameraUiState(
    val isLoading: Boolean = false,
    val isFailure: Boolean = false,
    val extractedText: String? = null,
    val successText: ItemResult? = null,
    val isSuccessScan: Boolean = false,
    val changeTheme:Boolean = false,
)
package com.calculator.scanner.presentation.welcome

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.google.accompanist.insets.statusBarsPadding
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.BVTTypography
import com.calculator.scanner.ui.theme.BlueOnesan2
import com.calculator.scanner.ui.theme.Neutral2
import com.calculator.scanner.R
@Composable
@Preview(showBackground = true)
private fun PreviewWelcomeScreen() {
    WelcomeScreen()
}

@Composable
fun WelcomeScreen(viewModel: WelcomeViewModel = hiltViewModel()) {
    val uiState = viewModel.uiState

    Column(
        modifier = Modifier
            .fillMaxSize()
            .statusBarsPadding()
            .background(BlueOnesan2),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            modifier = Modifier.fillMaxSize(0.55f),
            painter = painterResource(id = R.drawable.ic_icon_png),
            contentDescription = null,
            contentScale = ContentScale.Fit
        )

        CircularProgressIndicator(
            modifier = Modifier
                .size(50.dp),
            strokeWidth = 8.dp,
            color = Neutral2
        )

        Column(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            BVTText(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp),
                text = "Copyright © 2023 ${stringResource(id = R.string.app_name)}",
                color = Neutral2,
                textAlign = TextAlign.Center,
                style = BVTTypography.H5Bold,
                maxLines = 1,
            )
        }

    }
}
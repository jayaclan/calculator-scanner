package com.calculator.scanner.presentation.camera

import android.Manifest
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.calculator.scanner.R
import com.calculator.scanner.presentation.camera.component.TextRecognitionView
import com.calculator.scanner.ui.components.button.BVTButton
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.*
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionRequired
import com.google.accompanist.permissions.rememberPermissionState

@Composable
@Preview(showBackground = true)
fun CameraScreenPreview() {
    CameraScreen()
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun CameraScreen(
    viewModel: CameraViewModel = hiltViewModel(),
    onClickBack: () -> Unit = {},
    onClickScan: () -> Unit = {},
    onClickSetting: () -> Unit = {},
) {
    val context = LocalContext.current
    val uiState = viewModel.uiState
    val cameraPermissionState = rememberPermissionState(Manifest.permission.CAMERA)
    LaunchedEffect(true) {
        cameraPermissionState.launchPermissionRequest()
    }
    PermissionRequired(
        permissionState = cameraPermissionState,
        permissionNotGrantedContent = {
            Column(
                Modifier
                    .fillMaxSize()
                    .then(Modifier.padding(16.dp)),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(stringResource(R.string.camera_permission_info_0))
                Spacer(modifier = Modifier.height(8.dp))
                Row {
                    Button(onClick = {
                        cameraPermissionState.launchPermissionRequest()
                    }) {
                        Text(stringResource(R.string.camera_permission_grantbutton_0))
                    }
                }
            }
        },
        permissionNotAvailableContent = {
            Column(
                Modifier
                    .fillMaxSize()
                    .then(Modifier.padding(16.dp)),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(stringResource(R.string.camera_permission_info_1))
                Spacer(modifier = Modifier.height(8.dp))
                Row {
                    Button(onClick = {
                        onClickSetting()
                    }) {
                        Text(stringResource(R.string.camera_permission_grantbutton_1))
                    }
                }
            }
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(if (uiState.changeTheme) BlueOnesan2 else RedOneSan)
                .statusBarsPadding()
                .background(Platinum),
        ) {
            TextRecognitionView(
                viewModel = viewModel,
                onClickBack = onClickBack
            )

            BVTText(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
                    .background(Color.White),
                text = uiState.extractedText ?: "",
                color = TextBlack,
                style = BVTTypography.H3Regular,
                maxLines = 100,
            )


            Column(modifier = Modifier.fillMaxWidth()) {


                if (uiState.isSuccessScan)
                    BVTText(
                        modifier = Modifier
                            .fillMaxWidth(),
                        text = "Founded ${uiState.successText?.input}",
                        color = DusRed6,
                        style = BVTTypography.H3Regular,
                        maxLines = 1,
                    )

                BVTButton(
                    modifier = Modifier
                        .padding(horizontal = 4.dp)
                        .fillMaxWidth(),
                    text = "Succes Scan",
                    enabled = uiState.isSuccessScan,
                    type = if (uiState.changeTheme) BVTButton.BlueOneSan2 else BVTButton.RedOneSan2,
                    textAlign = TextAlign.Center,
                    onClick = {
                        onClickScan()
                    }
                )
            }
        }
    }

}

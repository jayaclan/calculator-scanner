package com.calculator.scanner.presentation.home.model

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ItemRepository(private val itemDatabaseDao: ItemDatabaseDao) {


    private val coroutineScope = CoroutineScope(Dispatchers.Main)

    fun getAll(): List<ItemResult> {
        return itemDatabaseDao.getAll()
    }

    suspend fun addItem(itemResult: ItemResult) {
        coroutineScope.launch(Dispatchers.IO) {
            itemDatabaseDao.addItem(itemResult)
        }

    }

    suspend fun updateItem(itemResult: ItemResult) {
        itemDatabaseDao.update(itemResult)
    }

    suspend fun deleteItem(itemResult: ItemResult) {
        itemDatabaseDao.delete(itemResult)
    }

    suspend fun deleteAllItem() {
        itemDatabaseDao.deleteAllTodos()
    }
}

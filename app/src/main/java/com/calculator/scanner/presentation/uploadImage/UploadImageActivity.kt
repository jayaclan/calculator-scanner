package com.calculator.scanner.presentation.uploadImage

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.Surface
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.lifecycleScope
import com.calculator.base.activity.BaseActivity
import com.calculator.scanner.presentation.camera.CameraScreen
import com.calculator.scanner.presentation.camera.CameraViewModel
import com.calculator.scanner.ui.theme.BVTComposeTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class UploadImageActivity : BaseActivity() {
    companion object {
        const val INTENT_ITEM = "INTENT_ITEM"
    }

    private val viewModel: UploadImageViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            viewModel.loadTheme()
        }

        setContent {
            BVTComposeTheme {
                Surface {
                    UploadImageScreen(
                        viewModel = viewModel,
                        onClickBack = {
                            onBackPressed()
                        },
                        onClickSetting = {
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri: Uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivity(intent)
                        },
                        onClickScan = {
                            val intent = Intent().apply { putExtra(INTENT_ITEM, viewModel.uiState.successText) }
                            setResult(RESULT_OK,intent)
                            finish()
                        }
                    )
                }
            }
        }
        setupObserver()
    }




    override fun setupObserver() {

    }

}
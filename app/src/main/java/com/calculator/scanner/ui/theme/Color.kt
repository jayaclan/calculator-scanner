package com.calculator.scanner.ui.theme

import androidx.compose.ui.graphics.Color

val Background = Color(0xFFF5F5F5)
val TextGrey = Color(0xFF646464)
val TextBlack = Color(0xFF000000)
val KemdikbudBlue = Color(0xFF0192D5)

val gradientBlueAllianz = Color(0xFF232F62)
val InturOrange2 = Color(0xFFFBA646)
val KemdikbudYellow = Color(0xFFFAD407)
val Primary1 = Color(0xFFE6F7FF)
val Primary6 = Color(0xFF1890FF)
val Neutral1 = Color(0xFFFFFFFF)
val Neutral2 = Color(0xFFFAFAFA)
val Neutral3 = Color(0xFFF5F5F5)
val Neutral4 = Color(0xFFF0F0F0)
val Neutral5 = Color(0xFFD9D9D9)
val Neutral6 = Color(0xFFBFBFBF)
val Neutral7 = Color(0xFF8C8C8C)
val Neutral8 = Color(0xFF595959)
val Neutral9 = Color(0xFF434343)
val Neutral10 = Color(0xFF262626)

val BlueOnesan = Color(0xFF86CECB)
val Platinum = Color(0xFF262626)
val GrayOneSan = Color(0xFFBEC8D1)
val BlueOnesan2 = Color(0xFF137A7F)
val RedOneSan = Color(0xFFE12885)
val Platinum2 = Color(0xFF302F2F)



val DikbudShade4 = Color(0xFF72CDF6)
val Global07 = Color(0xFFF8F9FA)
val DusRed5 = Color(0xFFFF4D4F)
val DusRed6 = Color(0xFFF5222D)
val KemendikbudGreen = Color(0xFF73D13D)
val Dot = Color(0xFFCFE2E1)
val GreyBackGround = Color(0xFFEEEEEE)
val Grey2 = Color(0xFFC4C4C4)
val Grey3 = Color(0xFFCCCCCC)


val CharacterDisabled = Color(0x40000000)

val Gradient1 = listOf(KemdikbudBlue, Color(0xFF70CEFA))
val Gradient2 = listOf(KemdikbudBlue, Color(0xFF1A4457))
val Gradient3 = listOf(BlueOnesan, Color(0xFFC67C28))







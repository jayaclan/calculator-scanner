package com.calculator.scanner.ui.components.dropdown

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.DropdownMenu
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.PopupProperties
import com.calculator.scanner.R
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.BVTTypography
import com.calculator.scanner.ui.theme.CharacterDisabled
import com.calculator.scanner.ui.theme.Neutral3
import com.calculator.scanner.ui.theme.Neutral5

@Composable
fun BVTDropDown2(
    modifier: Modifier = Modifier,
    text: String = "",
    hint: String = "",
    enabled: Boolean = true,
    suggestions: List<String> = listOf(),
    onItemClick: (Int) -> Unit = {},
) {
    val localDensity = LocalDensity.current
    var menuWidthSize by remember { mutableStateOf(0.dp) }
    var expanded by remember { mutableStateOf(false) }

    Column(
        modifier
            .onGloballyPositioned {
                with(localDensity) { menuWidthSize = it.size.width.toDp() }
            }
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(40.dp)
                .border(1.dp, Neutral5, RoundedCornerShape(2.dp))
                .then(
                    if (enabled) Modifier.clickable { expanded = !expanded }
                    else Modifier.background(Neutral3)
                ),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            BVTText(
                style = BVTTypography.H5Regular,
                text = text.ifEmpty { hint },
                color = if (text.isEmpty() || !enabled) CharacterDisabled.copy(0.25f)
                else CharacterDisabled.copy(0.85f),
                modifier = Modifier
                    .padding(vertical = 8.dp, horizontal = 12.dp)
                    .weight(1f)
            )
            Image(
                painter = painterResource(id = R.drawable.ic_arrow_down),
                contentDescription = null,
                modifier = Modifier
                    .padding(end = 16.dp)
                    .align(Alignment.CenterVertically)
                    .rotate(if (expanded) 180f else 360f)
            )
        }

        DropdownMenu(
            modifier = Modifier
                .width(menuWidthSize)
                .requiredSizeIn(maxHeight = 200.dp)
                .shadow(0.dp),
            expanded = suggestions.isNotEmpty() && expanded,
            onDismissRequest = { expanded = false },
            properties = PopupProperties(clippingEnabled = false),
        ) {
            suggestions.forEachIndexed { index, it ->
                BVTText(
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable {
                            onItemClick(index)
                            expanded = false
                        }
                        .padding(horizontal = 12.dp, vertical = 8.dp),
                    text = it
                )
            }
        }
    }

}
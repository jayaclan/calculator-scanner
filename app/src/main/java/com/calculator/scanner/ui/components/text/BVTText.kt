package com.calculator.scanner.ui.components.text

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Divider
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.calculator.base.BuildConfig
import com.calculator.scanner.ui.theme.*

@Composable
@Preview(showBackground = true, device = Devices.PIXEL_2)
private fun PreviewBVTText() {
    val text = "Lorem ipsum dolor sit amet"
    Column(Modifier.fillMaxSize()) {
        BVTText(text = text, style = BVTTypography.KemdikbudTitle)
        BVTText(text = text, style = BVTTypography.HeadingH4Bold)
        BVTText(text = text, style = BVTTypography.Body2)
        BVTText(text = text, style = BVTTypography.H5Medium)
        BVTText(text = text, style = BVTTypography.H5Regular)
        BVTText(text = text, style = BVTTypography.H5Bold)
        Divider(color = (Color.Red))
        BVTText(text = text, type = BVTText.Gradient, style = BVTTypography.HeadingH4Bold)
    }
}


@Composable
fun BVTTextFieldMultiLine(
    modifier: Modifier = Modifier,
    text: String = if (BuildConfig.DEBUG) "" else "",
    keyboardType: KeyboardType = KeyboardType.Text,
    imeAction: ImeAction = ImeAction.Default,
    enabled: Boolean = true,
    onChange: (String) -> Unit = {},
    onActionClick: () -> Unit = {},
    leadingIcon: @Composable (() -> Unit)? = null,
    placeholder: String = "",
    textStyle: TextStyle = BVTTypography.H5Medium,
) {
    var txt by remember { mutableStateOf(text) }

    OutlinedTextField(
        modifier = modifier,
        textStyle = textStyle,
        value = txt,
        enabled = enabled,
        placeholder = {
            if (placeholder.isNotEmpty())
                BVTText(
                    text = placeholder,
                    style = textStyle,
                    color = Neutral2.copy(0.6f)
                )
        },
        onValueChange = {
            txt = it
            onChange(it)
        },
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Platinum,
            focusedIndicatorColor = BlueOnesan,
            focusedLabelColor = BlueOnesan,
            cursorColor = BlueOnesan,
            unfocusedIndicatorColor = BlueOnesan2,
            textColor = Neutral2,
            placeholderColor = Neutral2,
        ),

        shape = RoundedCornerShape(8.dp),
        visualTransformation = VisualTransformation.None,
        keyboardOptions = KeyboardOptions(keyboardType = keyboardType, imeAction = imeAction),
        keyboardActions = KeyboardActions(
            onDone = {
                onActionClick()
            },
            onGo = {
                onActionClick()
            },
            onNext = {
                onActionClick()
            },
            onPrevious = {
                onActionClick()
            },
            onSearch = {
                onActionClick()
            },
            onSend = {
                onActionClick()
            },
        ),
        leadingIcon = leadingIcon,
    )
}

@Composable
fun BVTText(
    modifier: Modifier = Modifier,
    text: String = "",
    style: TextStyle = BVTTypography.H5Regular,
    type: BVTText = BVTText.Default,
    color: Color = Neutral10,
    maxLines: Int = 10,
    textAlign: TextAlign = TextAlign.Start,
    onTextLayout: (TextLayoutResult) -> Unit = {},
    textDecoration: TextDecoration? = null,
) {
//    var textModifier = modifier
//    if (type == BVTText.Gradient)
//        textModifier = textModifier.textGradient()
    Text(
        modifier = modifier,
        text = text,
        style = style,
        color = if (type == BVTText.Gradient) BlueOnesan else color,
        maxLines = maxLines,
        overflow = TextOverflow.Ellipsis,
        textAlign = textAlign,
        letterSpacing = style.letterSpacing,
        onTextLayout = onTextLayout,
        textDecoration = textDecoration
    )
}

enum class BVTText {
    Default,
    Gradient
}

//private fun Modifier.textGradient(gradient: List<Color> = Gradient2) = this
//    .graphicsLayer(alpha = 0.99f)
//    .drawWithCache {
//        val brush = Brush.linearGradient(
//            colors = gradient,
//            start = Offset.Zero,
//            end = Offset(135f, size.width / 2 + size.height / 2),
//            tileMode = TileMode.Clamp
//        )
//        onDrawWithContent {
//            drawContent()
//            drawRect(brush, blendMode = BlendMode.SrcAtop)
//        }
//    }

package com.calculator.scanner.ui.components.dialog

import android.app.DatePickerDialog
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.calculator.scanner.R
import java.util.*

@Composable
fun BVTDatePicker(
    onDateChangeListener: (year: Int, month: Int, day: Int) -> Unit
): DatePickerDialog {
    val mContext = LocalContext.current
    val mCalendar = Calendar.getInstance()
    val defaultYear = mCalendar.get(Calendar.YEAR)
    val defaultMonth = mCalendar.get(Calendar.MONTH)
    val defaultDay = mCalendar.get(Calendar.DAY_OF_MONTH)

    val mDatePickerDialog = DatePickerDialog(
        mContext, R.style.DatePicker, {_, year, month, dayOfMonth ->
            onDateChangeListener( year, month, dayOfMonth)
        }, defaultYear, defaultMonth, defaultDay
    )
    return mDatePickerDialog
}

package com.calculator.scanner.ui.components.dialog

import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.calculator.scanner.ui.components.button.BVTButton
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.*
import com.calculator.scanner.R
@Composable
@Preview(showBackground = true)
private fun PreviewBVTDialog() {
    BVTDialog(
        title = "Peringatan",
        icon = R.drawable.ic_warning,
        typeButtonPrimary = BVTButton.Red,
        textButtonPrimary = "Lanjutkan",
//        typeButtonSecondary = BVTButton.OrangeOutline,
//        textButtonSecondary = "Batalkan",
        statusShow = true,
        dialogContent = {
            BVTText(
                text = "Data yang telah anda ubah tidak akan tersimpan apabila anda melanjutkan",
                style = BVTTypography.BodyRegular,
                color = Neutral8,
            )
        }
    )
}

@Composable
fun BVTDialog(
    title: String = "",
    @DrawableRes icon: Int = R.drawable.ic_warning,
    dialogContent: @Composable () -> Unit,
    textButtonPrimary: String = "",
    typeButtonPrimary: BVTButton = BVTButton.Green,
    textButtonSecondary: String? = null,
    typeButtonSecondary: BVTButton = BVTButton.Primary,
    onClickPrimary: () -> Unit = {},
    onClickSecondary: () -> Unit = {},
    onClickDismiss: () -> Unit = { onClickPrimary() },
    statusShow: Boolean = false,
    isCancelable: Boolean = true,
    isPrimaryButtonEnable: Boolean = true
) {

    if (statusShow) {
        Dialog(
            onDismissRequest = onClickDismiss,
            properties = if (isCancelable) DialogProperties()
            else DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false)
        ) {
            Surface(
                modifier = Modifier.defaultMinSize(minWidth = 345.dp),
                shape = RoundedCornerShape(10.dp),
                color = Neutral1
            ) {
                Column(
                    Modifier
                        .background(Platinum)
                        .padding(16.dp)
                        .fillMaxWidth()
                ) {
                    BVTText(
                        modifier = Modifier.fillMaxWidth(),
                        text = title,
                        style = BVTTypography.H5Bold,
                        textAlign = TextAlign.Center,
                        color = BlueOnesan
                    )

                    Column(modifier = Modifier.padding(top = 8.dp)) {
                        dialogContent()
                    }

                    Row(
                        modifier = Modifier
                            .padding(top = 24.dp)
                            .fillMaxWidth(),
                        horizontalArrangement = if (textButtonSecondary != null) Arrangement.SpaceBetween else Arrangement.Center
                    ) {

                        if (textButtonSecondary != null) {
                            BVTButton(
                                text = textButtonSecondary,
                                modifier = Modifier.padding(end = 8.dp),
                                type = typeButtonSecondary,
                                onClick = {
                                    onClickSecondary()
                                },
                                textAlign = TextAlign.Center
                            )
                        }

                        BVTButton(
                            text = textButtonPrimary,
                            type = typeButtonPrimary,
                            onClick = {
                                onClickPrimary()
                            },
                            enabled = isPrimaryButtonEnable
                        )
                    }
                }
            }
        }
    }
}

package com.calculator.scanner.ui.components.bottomnav

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.BVTTypography
import com.calculator.scanner.ui.theme.BlueOnesan
import com.calculator.scanner.ui.theme.Neutral5
import com.calculator.scanner.ui.theme.Platinum


@Composable
@Preview(showBackground = true)
private fun PreviewBVTBottomNav() {
    Row(Modifier.fillMaxWidth()) {
        BVTBottomNav(
            menus = listOf(
                BottomNavMenus.Home,
                BottomNavMenus.MyListPokemon
            )
        )
    }
}

@Composable
fun BVTBottomNav(
    selectedRoute: String = BottomNavMenus.Home.route,
    menus: List<BottomNavMenus> = listOf(),
    onMenuClicked: (BottomNavMenus) -> Unit = {}
) {
    Column(
        Modifier
            .fillMaxWidth()
            .background(Platinum)
    ) {
        Row(Modifier.fillMaxWidth()) {
            menus.forEach { menu ->
                Divider(
                    modifier = Modifier.weight(1f),
                    color = if (selectedRoute == menu.route) BlueOnesan else Platinum,
                    thickness = 4.dp
                )
            }
        }

        BottomNavigation(backgroundColor = Platinum, elevation = 0.dp) {
            menus.forEach { menu ->
                val isSelected = selectedRoute == menu.route
                val color = if (isSelected) BlueOnesan else Neutral5

                BottomNavigationItem(
                    label = {
                        BVTText(
                            text = menu.title,
                            style = BVTTypography.Label,
                            color = color
                        )
                    },
                    selected = isSelected,
                    icon = {
                        Icon(
                            painter = painterResource(id = if (isSelected) menu.activeRes else menu.unActiveRes),
                            contentDescription = menu.route,
                            tint = color
                        )
                    },
                    onClick = {
                        onMenuClicked(menu)
                    }
                )
            }
        }
    }
}
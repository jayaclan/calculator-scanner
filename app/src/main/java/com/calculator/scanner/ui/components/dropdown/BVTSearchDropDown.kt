package com.calculator.scanner.ui.components.dropdown

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActionScope
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.takeOrElse
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.PopupProperties
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.*

@Composable
@Preview(showBackground = true)
fun BVTSearchDropDown(
    modifier: Modifier = Modifier,
    text: String = "",
    hint: String = "",
    singleLine: Boolean = true,
    isPassword: Boolean = false,
    keyboardType: KeyboardType = KeyboardType.Text,
    imeAction: ImeAction = ImeAction.Default,
    enabled: Boolean = true,
    errorMessage: String? = null,
    suggestions: List<String> = listOf(),
    onTextChange: (String) -> Unit = {},
    onItemClick: (Int) -> Unit = {},
    onActionClick: () -> Unit = {},
) {
    val localDensity = LocalDensity.current
    var menuWidthSize by remember { mutableStateOf(0.dp) }
    var expanded by remember { mutableStateOf(false) }
    val actionClick: (KeyboardActionScope.() -> Unit) = { onActionClick() }

    Column(
        modifier
            .onGloballyPositioned {
                with(localDensity) { menuWidthSize = it.size.width.toDp() }
            }
    ) {
        OutlinedCustomTextField(
            modifier = Modifier
                .fillMaxWidth()
                .onFocusChanged {
                    expanded = it.isFocused
                },
            textStyle = BVTTypography.H5Regular,
            value = text,
            isError = errorMessage != null,
            enabled = enabled,
            onValueChange = {
                onTextChange(it)
            },
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Neutral1,
                focusedIndicatorColor = BlueOnesan,
                focusedLabelColor = BlueOnesan,
                cursorColor = BlueOnesan,
                unfocusedIndicatorColor = Neutral5,
            ),
            placeholder = {
                BVTText(text = hint, style = BVTTypography.H5Regular, color = CharacterDisabled)
            },
            shape = RoundedCornerShape(2.dp),
            keyboardOptions = KeyboardOptions(
                keyboardType = keyboardType,
                imeAction = imeAction
            ),
            keyboardActions = KeyboardActions(
                onDone = actionClick,
                onGo = actionClick,
                onNext = actionClick,
                onPrevious = actionClick,
                onSearch = actionClick,
                onSend = actionClick,
            ),
            singleLine = singleLine,
            trailingIcon = {
                Icon(
                    imageVector = Icons.Default.ArrowDropDown,
                    tint = TextGrey,
                    contentDescription = null
                )
            },
        )
        if (errorMessage != null) {
            BVTText(
                modifier = Modifier.padding(start = 14.dp),
                text = errorMessage, style = TextStyle(
                    fontFamily = sourceSansPro,
                    fontWeight = FontWeight.W400,
                    fontSize = 12.sp,
                ),
                color = Color.Red
            )
        }
        if (suggestions.isNotEmpty() && expanded) {
            DropdownMenu(
                modifier = Modifier
                    .width(menuWidthSize)
                    .requiredSizeIn(maxHeight = 200.dp)
                    .shadow(0.dp),
                expanded = true,
                onDismissRequest = {
                },
                properties = PopupProperties(clippingEnabled = false),
            ) {
                suggestions.forEachIndexed { index, item ->
                    BVTText(
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable { onItemClick(index) }
                            .padding(horizontal = 12.dp, vertical = 8.dp),
                        text = item
                    )
                }
            }
        }
    }
}

/**
 * The code below is an androidx.compose.material.OutlinedTextField
 * whose padding has been changed
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun OutlinedCustomTextField(
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = LocalTextStyle.current,
    label: @Composable (() -> Unit)? = null,
    placeholder: @Composable (() -> Unit)? = null,
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    singleLine: Boolean = false,
    maxLines: Int = Int.MAX_VALUE,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    shape: Shape = MaterialTheme.shapes.small,
    colors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors()
) {
    // If color is not provided via the text style, use content color as a default
    val textColor = textStyle.color.takeOrElse {
        colors.textColor(enabled).value
    }
    val mergedTextStyle = textStyle.merge(TextStyle(color = textColor))

    BasicTextField(
        value = value,
        modifier = modifier
            .background(colors.backgroundColor(enabled).value, shape),
        onValueChange = onValueChange,
        enabled = enabled,
        readOnly = readOnly,
        textStyle = mergedTextStyle,
        cursorBrush = SolidColor(colors.cursorColor(isError).value),
        visualTransformation = visualTransformation,
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
        interactionSource = interactionSource,
        singleLine = singleLine,
        maxLines = maxLines,
        decorationBox = @Composable { innerTextField ->
            TextFieldDefaults.OutlinedTextFieldDecorationBox(
                value = value,
                visualTransformation = visualTransformation,
                innerTextField = innerTextField,
                placeholder = placeholder,
                label = label,
                leadingIcon = leadingIcon,
                trailingIcon = trailingIcon,
                singleLine = singleLine,
                enabled = enabled,
                isError = isError,
                interactionSource = interactionSource,
                colors = colors,
                contentPadding = PaddingValues(16.dp, 8.dp, 16.dp, 8.dp),
                border = {
                    TextFieldDefaults.BorderBox(
                        enabled,
                        isError,
                        interactionSource,
                        colors,
                        shape
                    )
                }
            )
        }
    )
}
package com.calculator.scanner.ui.components.dialog

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.BVTTypography
import com.calculator.scanner.ui.theme.Neutral1


@Composable
fun BVTChoosePhoto(
    statusShow: Boolean = false,
    onClickTakePhoto: () -> Unit = {},
    onClickChooseGallery: () -> Unit = {},
    onDismiss: () -> Unit = {},
) {
    if (statusShow) {
        Dialog(onDismissRequest = onDismiss) {
            Surface(
                modifier = Modifier
                    .width(210.dp),
                shape = RoundedCornerShape(2.dp),
                color = Neutral1
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    BVTText(
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable(onClick = onClickTakePhoto)
                            .padding(vertical = 9.dp)
                            .padding(horizontal = 12.dp),
                        text = "Input Form Camera",
                        style = BVTTypography.BodyRegular
                    )
                    BVTText(
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable(onClick = onClickChooseGallery)
                            .padding(vertical = 9.dp)
                            .padding(horizontal = 12.dp),
                        text = "Input Form Gallery",
                        style = BVTTypography.BodyRegular
                    )
                }
            }

        }
    }
}
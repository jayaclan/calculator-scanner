package com.calculator.scanner.ui.components.dialog

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.window.Dialog
import coil.compose.rememberAsyncImagePainter


@Composable
fun BVTPreviewPhoto(
    statusShow: Boolean = false,
    onDismiss: () -> Unit = {},
    imageUrl: String
) {
    if (statusShow) {
        Dialog(onDismissRequest = onDismiss) {
//            Card(
//                shape = RoundedCornerShape(10.dp),
//                elevation = 0.dp
//            ) {
//
//            }
            Image(
                modifier = Modifier.clickable { onDismiss() },
                painter = rememberAsyncImagePainter(imageUrl), contentDescription = null,
                contentScale = ContentScale.Fit
            )

        }
    }
}
package com.calculator.scanner.ui.components.topbar

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.material.Switch
import androidx.compose.material.SwitchColors
import androidx.compose.material.SwitchDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.calculator.scanner.R
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.BVTTypography
import com.calculator.scanner.ui.theme.BlueOnesan2
import com.calculator.scanner.ui.theme.Neutral2
import com.calculator.scanner.ui.theme.RedOneSan

@Composable
@Preview(showBackground = true)
fun TopBarIconic(
    modifier: Modifier = Modifier,
    backIcon: Boolean = false,
    onClickBack: () -> Unit = {},
    onClickTheme: (Boolean) -> Unit = {},
    darkTheme: Boolean = true,
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Row(
                modifier = Modifier
                    .weight(1f)
                    .padding(end = 8.dp)
                    .background(if (darkTheme) BlueOnesan2 else RedOneSan,
                        CutCornerShape(bottomEnd = 48.dp)),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                if (backIcon)
                    Image(
                        modifier = Modifier
                            .clickable {
                                onClickBack()
                            }
                            .padding(8.dp),
                        painter = painterResource(id = R.drawable.ic_left_new),
                        contentDescription = null,
                        contentScale = ContentScale.FillWidth,
                        colorFilter = ColorFilter.tint(Neutral2)
                    )

                Image(
                    modifier = Modifier
                        .size(40.dp)
                        .padding(start = 8.dp)
                        .padding(vertical = 2.dp),
                    painter = painterResource(id = R.drawable.ic_icon_png),
                    contentDescription = null,
                    contentScale = ContentScale.Fit,
                    colorFilter = ColorFilter.tint(Neutral2)
                )

                BVTText(
                    modifier = Modifier
                        .weight(1f)
                        .padding(top = 6.dp),
                    text = stringResource(R.string.app_name),
                    color = Neutral2,
                    style = BVTTypography.H4Bold,
                    maxLines = 1,
                )

            }

            Column(
                modifier = Modifier
                    .padding(end = 12.dp)
                    .size(28.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                BVTText(
                    modifier = Modifier
                        .weight(1f),
                    text = "Theme",
                    color = Neutral2,
                    style = BVTTypography.Label.copy(
                        fontSize = 8.sp,
                    ),
                    maxLines = 1,
                )
                Switch(
                    modifier = Modifier.size(15.dp),
                    checked = darkTheme,
                    onCheckedChange = {
                        onClickTheme(it)
                    },
                    colors = SwitchDefaults.colors(
                        checkedThumbColor = BlueOnesan2,
                        checkedTrackColor = BlueOnesan2.copy(0.7f),
                        uncheckedThumbColor = RedOneSan,
                        uncheckedTrackColor= RedOneSan.copy(0.7f),
                    ),

                    )
            }
        }


    }
}
package com.calculator.scanner.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.calculator.scanner.R.font.*


val sourceSansPro = FontFamily(
    /** 200 **/
    Font(sourcesanspro_extra_light, FontWeight.W200),
    /** 200 **/
    Font(sourcesanspro_extra_light_italic, FontWeight.W200, FontStyle.Italic),
    /** 300 **/
    Font(sourcesanspro_light, FontWeight.W300),
    /** 300 **/
    Font(sourcesanspro_light_italic, FontWeight.W300, FontStyle.Italic),
    /** 400 **/
    Font(sourcesanspro_regular, FontWeight.W400),
    /** 400 **/
    Font(sourcesanspro_italic, FontWeight.W400, FontStyle.Italic),
    /** 600 **/
    Font(sourcesanspro_semi_bold, FontWeight.W600),
    /** 600 **/
    Font(sourcesanspro_semi_bold_italic, FontWeight.W600, FontStyle.Italic),
    /** 700 **/
    Font(sourcesanspro_bold, FontWeight.W700),
    /** 700 **/
    Font(sourcesanspro_bold_italic, FontWeight.W700, FontStyle.Italic),
    /** 900 **/
    Font(sourcesanspro_black, FontWeight.W900),
    /** 900 **/
    Font(sourcesanspro_black_italic, FontWeight.W900, FontStyle.Italic),
)

val poppins = FontFamily(
    /** 200 **/
    Font(poppins_extra_light, FontWeight.W200),
    /** 200 **/
    Font(poppins_extra_light_ltalic, FontWeight.W200, FontStyle.Italic),
    /** 300 **/
    Font(poppins_light, FontWeight.W300),
    /** 300 **/
    Font(poppins_light_italic, FontWeight.W300, FontStyle.Italic),
    /** 400 **/
    Font(poppins_regular, FontWeight.W400),
    /** 400 **/
    Font(poppins_italic, FontWeight.W400, FontStyle.Italic),
    /** 600 **/
    Font(poppins_medium, FontWeight.W600),
    /** 600 **/
    Font(poppins_medium_italic, FontWeight.W600, FontStyle.Italic),
    /** 700 **/
    Font(poppins_bold, FontWeight.W700),
    /** 700 **/
    Font(poppins_bold_italic, FontWeight.W700, FontStyle.Italic),
    /** 900 **/
    Font(poppins_black, FontWeight.W900),
    /** 900 **/
    Font(poppins_black_italic, FontWeight.W900, FontStyle.Italic),
)

val BVTTypography = CustomTypography()

class CustomTypography(
    // ==== Source Sans Pro ====
    val KemdikbudTitle: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W700,
        fontSize = 48.sp,
    ),

    val H5Medium: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W600,
        fontSize = 14.sp,
    ),
    val H5Regular: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W400,
        fontSize = 14.sp,
    ),

    val H5Bold: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W700,
        fontSize = 14.sp,
    ),
    val HeadingH4Bold: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W700,
        fontSize = 24.sp,
    ),
    val H4Bold: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W700,
        fontSize = 16.sp,
    ),
    val H4Medium: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W600,
        fontSize = 16.sp,
    ),
    val H3Regular: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W400,
        fontSize = 20.sp,
    ),
    val H3Medium: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W600,
        fontSize = 20.sp,
    ),
    val H3Bold: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W700,
        fontSize = 20.sp,
    ),
    val H2Medium: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W600,
        fontSize = 26.sp,
    ),
    val H2Bold: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W700,
        fontSize = 26.sp,
    ),
    val Body2: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W400,
        fontSize = 14.sp,
    ),
    val BodyBold: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W700,
        fontSize = 12.sp,
    ),
    val BodyMedium: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W600,
        fontSize = 12.sp,
    ),
    val BodyRegular: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W400,
        fontSize = 12.sp,
    ),

    val LabelBold: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W700,
        fontSize = 10.sp,
    ),
    val Label: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W400,
        fontSize = 10.sp,
    ),
    val FootnoteDesc: TextStyle = TextStyle(
        fontFamily = poppins,
        fontWeight = FontWeight.W600,
        fontSize = 10.sp,
    ),
)

val Typography = Typography(
    h4 = BVTTypography.H5Regular,
    body2 = BVTTypography.Body2,
    subtitle1 = BVTTypography.H5Bold,
    button = BVTTypography.H5Regular
)

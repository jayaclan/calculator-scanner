package com.calculator.scanner.ui.components.checkbox


import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Checkbox
import androidx.compose.material.CheckboxDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.calculator.scanner.ui.theme.BlueOnesan
import com.calculator.scanner.ui.theme.Neutral5


@Composable
@Preview(showBackground = true)
private fun PreviewBVTRadioButton() {
    BVTCheckBox(
        modifier = Modifier.fillMaxWidth(),
        onCheckedChange = {

        }
    )
}


@Composable
fun BVTCheckBox(
    modifier: Modifier,
    value: Boolean = false,
    onCheckedChange: (status: Boolean) -> Unit
) {
    val checkedState = remember { mutableStateOf(value) }
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Checkbox(
            checked = checkedState.value,
            onCheckedChange = {
                checkedState.value = it
                onCheckedChange(it)
            },
            colors = CheckboxDefaults.colors(
                checkedColor = BlueOnesan,
                uncheckedColor = Neutral5,
            )
        )
    }
}
package com.calculator.scanner.ui.components.bottomnav

import androidx.annotation.DrawableRes
import com.calculator.scanner.R


sealed class BottomNavMenus(
    val route: String,
    val title: String,
    @DrawableRes val unActiveRes: Int,
    @DrawableRes val activeRes: Int,
) {
    object Home : BottomNavMenus(
        route = "Home",
        title = "Home",
        unActiveRes = R.drawable.ic_home,
        activeRes = R.drawable.ic_home_fill
    )

    object MyListPokemon : BottomNavMenus(
        route = "My Pokemon",
        title = "My Pokemon",
        unActiveRes = R.drawable.ic_information,
        activeRes = R.drawable.ic_information
    )
}

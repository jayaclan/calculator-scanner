package com.calculator.scanner.ui.components.radiobutton


import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.RadioButton
import androidx.compose.material.RadioButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.BVTTypography
import com.calculator.scanner.ui.theme.BlueOnesan
import com.calculator.scanner.ui.theme.CharacterDisabled
import com.calculator.scanner.ui.theme.Neutral5

@Composable
@Preview(showBackground = true)
private fun PreviewBVTRadioButton() {
    val options = listOf("Perempuan", "Laki Laki")
    BVTRadioButton(
        modifier = Modifier.fillMaxWidth(),
        selectedPos = 0,
        options = options
    )
}


@Composable
fun BVTRadioButton(
    modifier: Modifier = Modifier,
    selectedPos: Int = -1,
    options: List<String> = listOf(),
    onItemSelected: (index: Int, text: String) -> Unit = { _, _ -> },
    enabled: Boolean = true
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        options.forEachIndexed { index, it ->
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                RadioButton(
                    selected = selectedPos == index,
                    onClick = {
                        onItemSelected(index, it)
                    },
                    colors = RadioButtonDefaults.colors(
                        selectedColor = BlueOnesan,
                        unselectedColor = Neutral5.copy(alpha = 0.6f)
                    ),
                    enabled = enabled
                )
                BVTText(
                    modifier = Modifier.clickable {
                        if (enabled) onItemSelected(index, it)
                    },
                    style = if (enabled) BVTTypography.BodyRegular else BVTTypography.BodyRegular,
                    color = CharacterDisabled.copy(0.85f),
                    text = it,
                )
            }
        }
    }
}
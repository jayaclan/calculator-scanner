package com.calculator.scanner.ui.components.button

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.OutlinedButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.viewbinding.BuildConfig
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.*

@Composable
@Preview(showBackground = true)
private fun PreviewBVTButton() {
    Column(
        Modifier
            .fillMaxWidth()
            .padding(50.dp)
    ) {
        BVTButton(Modifier.fillMaxWidth(), type = BVTButton.Primary)
        BVTButton(Modifier.fillMaxWidth(), type = BVTButton.Red)
        BVTButton(Modifier.fillMaxWidth(), type = BVTButton.Secondary)
        BVTButton(type = BVTButton.Primary)
        BVTButton(type = BVTButton.Secondary)
        BVTButton(type = BVTButton.OrangeOutline)
        BVTButton(type = BVTButton.Primary, enabled = false)
        BVTButton(type = BVTButton.Secondary, enabled = false)
    }
}

@Composable
fun BVTButton(
    modifier: Modifier = Modifier,
    text: String = if (BuildConfig.DEBUG) "Lorem Ipsum" else "",
    type: BVTButton = BVTButton.Primary,
    enabled: Boolean = true,
    onClick: () -> Unit = {},
    textAlign: TextAlign = TextAlign.Start,
) {
    var buttonModifier = modifier
    if (!enabled) buttonModifier = buttonModifier.alpha(0.4f)
    OutlinedButton(
        modifier = buttonModifier,
        enabled = enabled,
        onClick = onClick,
        shape = RoundedCornerShape(5.dp),
        colors = ButtonDefaults.outlinedButtonColors(type.buttonColor),
        elevation = ButtonDefaults.elevation(
            defaultElevation = 0.dp,
            disabledElevation = 0.dp
        ),
        border = if (type == BVTButton.Secondary) BorderStroke(0.dp, Neutral6)
        else if (type == BVTButton.OrangeOutline) BorderStroke(1.dp, BlueOnesan) else null
    ) {
        BVTText(
            text = text,
            style = type.style,
            color = type.textColor,
            textAlign = textAlign
        )
    }
}

sealed class BVTButton(
    val style: TextStyle,
    val buttonColor: Color,
    val textColor: Color,
) {
    object Primary : BVTButton(
        style = BVTTypography.H5Regular,
        buttonColor = BlueOnesan,
        textColor = Neutral1,
    )

    object Secondary : BVTButton(
        style = BVTTypography.H5Regular,
        buttonColor = Color.Transparent,
        textColor = Neutral6,
    )

    object OrangeOutline : BVTButton(
        style = BVTTypography.H5Regular,
        buttonColor = Color.Transparent,
        textColor = BlueOnesan,
    )

    object Red : BVTButton(
        style = BVTTypography.H5Regular,
        buttonColor = DusRed6,
        textColor = Neutral1,
    )


    object BlueOneSan : BVTButton(
        style = BVTTypography.H5Regular,
        buttonColor = Neutral2,
        textColor = BlueOnesan,
    )

    object BlueOneSan2 : BVTButton(
        style = BVTTypography.H5Regular,
        buttonColor = BlueOnesan2,
        textColor = Neutral2,
    )

    object BlueOneSan2Reverse : BVTButton(
        style = BVTTypography.H5Regular,
        buttonColor = Neutral2,
        textColor = BlueOnesan2,
    )

    object RedOneSan2 : BVTButton(
        style = BVTTypography.H5Regular,
        buttonColor = RedOneSan,
        textColor = Neutral2,
    )

    object RedOneSan2Reverse : BVTButton(
        style = BVTTypography.H5Regular,
        buttonColor = Neutral2,
        textColor = RedOneSan,
    )

    object Red5Medium : BVTButton(
        style = BVTTypography.H5Medium,
        buttonColor = DusRed5,
        textColor = Neutral1,
    )

    object Green : BVTButton(
        style = BVTTypography.H5Regular,
        buttonColor = KemendikbudGreen,
        textColor = Neutral1,
    )

    object White : BVTButton(
        style = BVTTypography.H5Regular,
        buttonColor = Neutral1,
        textColor = TextBlack,
    )
}
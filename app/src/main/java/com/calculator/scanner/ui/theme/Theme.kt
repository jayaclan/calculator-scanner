package com.calculator.scanner.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val darkColorScheme = lightColors(
    primary = BlueOnesan2,
    primaryVariant = InturOrange2,
    secondary = Global07,
    background = Platinum
)

private val lightColorScheme = lightColors(
    primary = RedOneSan,
    primaryVariant = InturOrange2,
    secondary = Global07,
    background = Platinum
)

@Composable
fun BVTComposeTheme(
    isDarkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {

    val colorScheme = if (isDarkTheme) darkColorScheme else lightColorScheme
    MaterialTheme(
        colors = colorScheme,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}
package com.calculator.scanner.ui.components.textfield


import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActionScope
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.calculator.scanner.BuildConfig
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.*
import com.calculator.scanner.R
@Composable
@Preview(showBackground = true)
private fun PreviewBVTTextField2() {
    Column(
        Modifier
            .fillMaxWidth()
            .padding(50.dp)
    ) {
        val modifier = Modifier.fillMaxWidth()
        BVTTextField2(modifier)
        BVTTextField2(modifier, errorMessage = "Error message here")
        BVTTextField2(
            modifier = modifier,
            hint = "Email",
            text = "lorem@ipsum.mail",
            keyboardType = KeyboardType.Email,
            leadingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.ic_user),
                    tint = BlueOnesan,
                    contentDescription = null
                )
            }
        )
        BVTTextField2(
            modifier = modifier,
            hint = "Secret",
            isPassword = true,
            leadingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.ic_lock),
                    tint = BlueOnesan,
                    contentDescription = null
                )
            }
        )
    }
}

@Composable
fun BVTTextField2(
    modifier: Modifier = Modifier,
    text: String = if (BuildConfig.DEBUG) "Lorem ipsum" else "",
    hint: String = if (BuildConfig.DEBUG) "Lorem ipsum" else "",
    singleLine: Boolean = true,
    isPassword: Boolean = false,
    allowClear: Boolean = true,
    keyboardType: KeyboardType = KeyboardType.Text,
    imeAction: ImeAction = ImeAction.Default,
    enabled: Boolean = true,
    onChange: (String) -> Unit = {},
    onActionClick: () -> Unit = {},
    leadingIcon: @Composable (() -> Unit)? = null,
    errorMessage: String? = null,
    enableRemoveText: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
) {
    var passwordVisible by remember { mutableStateOf(false) }
    val transformation = if (isPassword)
        if (passwordVisible) VisualTransformation.None
        else PasswordVisualTransformation()
    else
        VisualTransformation.None
    var height by remember { mutableStateOf(0) }
    val actionClick: (KeyboardActionScope.() -> Unit) = { onActionClick() }
    val rounded = if (text.isEmpty()) RoundedCornerShape(2.dp)
    else RoundedCornerShape(2.dp, 0.dp, 0.dp, 2.dp)

    Column(modifier = modifier) {
        Row(
            modifier = Modifier,
            verticalAlignment = Alignment.CenterVertically
        ) {
            OutlinedCustomTextField(
                modifier = Modifier.weight(1f),
                textStyle = BVTTypography.H5Regular,
                value = text,
                isError = errorMessage != null,
                enabled = enabled,
                onValueChange = {
                    onChange(it)
                },
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Neutral1,
                    focusedIndicatorColor = BlueOnesan,
                    focusedLabelColor = BlueOnesan,
                    cursorColor = BlueOnesan,
                    unfocusedIndicatorColor = Neutral5,
                ),
                placeholder = {
                    BVTText(text = hint, style = BVTTypography.H5Regular, color = CharacterDisabled)
                },
                shape = rounded,
                visualTransformation = transformation,
                keyboardOptions = KeyboardOptions(
                    keyboardType = keyboardType,
                    imeAction = imeAction
                ),
                keyboardActions = KeyboardActions(
                    onDone = actionClick,
                    onGo = actionClick,
                    onNext = actionClick,
                    onPrevious = actionClick,
                    onSearch = actionClick,
                    onSend = actionClick,
                ),
                singleLine = singleLine,
                leadingIcon = leadingIcon,
                interactionSource = interactionSource
            )
//
        }
    }
}

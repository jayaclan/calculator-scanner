package com.calculator.scanner.ui.components.search

import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.calculator.scanner.R
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.components.textfield.OutlinedCustomTextField
import com.calculator.scanner.ui.theme.*


@Composable
@Preview(showBackground = true)
fun SearchIconic(
    modifier: Modifier = Modifier,
    value: String = "",
    onValueChange: (String) -> Unit = {}
){
    OutlinedCustomTextField(
        modifier = modifier,
        value = value,
        placeholder = {
            BVTText(
                text = "Cari",
                style = BVTTypography.H5Regular,
                color = Neutral2.copy(0.6f)
            )
        },
        shape = CutCornerShape(bottomEnd = 48.dp),
        onValueChange = {
            onValueChange(it)

        },
        singleLine = true,
        textStyle = LocalTextStyle.current.copy(
//                        textAlign = TextAlign.Center,
            color = Neutral2,
            fontFamily = poppins,
            fontSize = 14.sp,
            fontWeight = FontWeight.W400
        ),
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Platinum,
            focusedIndicatorColor = BlueOnesan,
            focusedLabelColor = BlueOnesan,
            cursorColor = BlueOnesan,
            unfocusedIndicatorColor = BlueOnesan2,
            textColor = Neutral2,
            placeholderColor = Neutral2,
        ),
        leadingIcon = {
            Icon(
                modifier = Modifier
                    .size(24.dp),
                painter = painterResource(id = R.drawable.ic_search),
                tint = BlueOnesan,
                contentDescription = null
            )
        }
    )
}
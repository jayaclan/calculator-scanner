package com.calculator.scanner.ui.components.textfield

import androidx.compose.foundation.background
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActionScope
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.takeOrElse
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.calculator.scanner.BuildConfig
import com.calculator.scanner.R
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.*

@Composable
@Preview(showBackground = true)
private fun PreviewBVTTextField() {
    Column(
        Modifier
            .fillMaxWidth()
            .padding(50.dp)
    ) {
        val modifier = Modifier.fillMaxWidth()
        BVTTextField(modifier, enabled = false)
        BVTTextField(modifier, errorMessage = "Error message here")
        BVTTextField(
            modifier = modifier,
            hint = "Email",
            text = "lorem@ipsum.mail",
            keyboardType = KeyboardType.Email,
            leadingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.ic_user),
                    tint = BlueOnesan,
                    contentDescription = null
                )
            }
        )
        BVTTextField(
            modifier = modifier,
            hint = "Secret",
            isPassword = true,
            leadingIcon = {
                Icon(
                    painter = painterResource(id = R.drawable.ic_lock),
                    tint = BlueOnesan,
                    contentDescription = null
                )
            }
        )
    }
}

@Composable
fun BVTTextField(
    modifier: Modifier = Modifier,
    text: String = if (BuildConfig.DEBUG) "Lorem ipsum" else "",
    hint: String = if (BuildConfig.DEBUG) "Lorem ipsum" else "",
    singleLine: Boolean = true,
    isPassword: Boolean = false,
    allowClear: Boolean = true,
    keyboardType: KeyboardType = KeyboardType.Text,
    imeAction: ImeAction = ImeAction.Default,
    enabled: Boolean = true,
    onChange: (String) -> Unit = {},
    onActionClick: () -> Unit = {},
    leadingIcon: @Composable (() -> Unit)? = null,
    errorMessage: String? = null,
    enableRemoveText: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
) {
    var passwordVisible by remember { mutableStateOf(false) }
    val transformation = if (isPassword)
        if (passwordVisible) VisualTransformation.None
        else PasswordVisualTransformation()
    else
        VisualTransformation.None
    var height by remember { mutableStateOf(0) }
    val actionClick: (KeyboardActionScope.() -> Unit) = { onActionClick() }
    val rounded = if (text.isEmpty()) RoundedCornerShape(2.dp)
    else RoundedCornerShape(2.dp, 0.dp, 0.dp, 2.dp)

    Column(modifier = modifier) {
        Row(
            modifier = Modifier.onGloballyPositioned {
                height = it.size.height
            },
            verticalAlignment = Alignment.CenterVertically
        ) {
            OutlinedCustomTextField(
                modifier = Modifier.weight(1f),
                textStyle = BVTTypography.H5Regular,
                value = text,
                isError = errorMessage != null,
                enabled = enabled,
                onValueChange = {
                    onChange(it)
                },
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Platinum.copy(0.8f),
                    focusedIndicatorColor = BlueOnesan,
                    focusedLabelColor = BlueOnesan,
                    cursorColor = BlueOnesan,
                    unfocusedIndicatorColor = Platinum.copy(0.8f),
                    textColor = Neutral2,
                    placeholderColor = Neutral2
                ),
                placeholder = {
                    BVTText(text = hint, style = BVTTypography.H5Regular, color = CharacterDisabled)
                },
                shape = rounded,
                visualTransformation = transformation,
                keyboardOptions = KeyboardOptions(
                        keyboardType = keyboardType,
                        imeAction = imeAction
                ),
                keyboardActions = KeyboardActions(
                    onDone = actionClick,
                    onGo = actionClick,
                    onNext = actionClick,
                    onPrevious = actionClick,
                    onSearch = actionClick,
                    onSend = actionClick,
                ),
                singleLine = singleLine,
//                trailingIcon = {
//                    if (allowClear && text.isNotEmpty() && enableRemoveText)
//                        IconButton(onClick = { onChange("") }) {
//                            Icon(
//                                painter = painterResource(id = R.drawable.ic_close),
//                                tint = TextGrey,
//                                contentDescription = ""
//                            )
//                        }
//                },
                leadingIcon = leadingIcon,
                interactionSource = interactionSource
            )
//            if (isPassword && text.isNotEmpty()) {
//                val heightInDp = with(LocalDensity.current) { height.toDp() }
//                val borderColor =
//                    MaterialTheme.colors.onSurface.copy(alpha = TextFieldDefaults.UnfocusedIndicatorLineOpacity)
//                IconButton(
//                    modifier = Modifier
//                        .height(heightInDp)
//                        .border(1.dp, borderColor),
//                    onClick = { passwordVisible = !passwordVisible }) {
//                    Icon(
//                        modifier = Modifier
//                            .padding(horizontal = 12.dp, vertical = 13.dp)
//                            .size(14.dp),
//                        painter = painterResource(id = R.drawable.ic_eye),
//                        tint = Neutral8,
//                        contentDescription = null
//                    )
//                }
//            }
        }
        if (errorMessage != null) {
            BVTText(
                modifier = Modifier.padding(start = 14.dp),
                text = errorMessage, style = TextStyle(
                    fontFamily = sourceSansPro,
                    fontWeight = FontWeight.W400,
                    fontSize = 12.sp,
                ),
                color = Color.Red
            )
        }
    }
}

/**
 * The code below is an androidx.compose.material.OutlinedTextField
 * whose padding has been changed
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun OutlinedCustomTextField(
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = LocalTextStyle.current,
    label: @Composable (() -> Unit)? = null,
    placeholder: @Composable (() -> Unit)? = null,
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    singleLine: Boolean = false,
    maxLines: Int = Int.MAX_VALUE,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    shape: Shape = MaterialTheme.shapes.small,
    colors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors()
) {
    // If color is not provided via the text style, use content color as a default
    val textColor = textStyle.color.takeOrElse {
        colors.textColor(enabled).value
    }
    val mergedTextStyle = textStyle.merge(TextStyle(color = textColor))

    BasicTextField(
        value = value,
        modifier = modifier
            .background(colors.backgroundColor(enabled).value, shape),
        onValueChange = onValueChange,
        enabled = enabled,
        readOnly = readOnly,
        textStyle = mergedTextStyle,
        cursorBrush = SolidColor(colors.cursorColor(isError).value),
        visualTransformation = visualTransformation,
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
        interactionSource = interactionSource,
        singleLine = singleLine,
        maxLines = maxLines,
        decorationBox = @Composable { innerTextField ->
            TextFieldDefaults.OutlinedTextFieldDecorationBox(
                value = value,
                visualTransformation = visualTransformation,
                innerTextField = innerTextField,
                placeholder = placeholder,
                label = label,
                leadingIcon = leadingIcon,
                trailingIcon = trailingIcon,
                singleLine = singleLine,
                enabled = enabled,
                isError = isError,
                interactionSource = interactionSource,
                colors = colors,
                contentPadding = PaddingValues(4.dp, 8.dp, 4.dp, 8.dp),
                border = {
                    TextFieldDefaults.BorderBox(
                        enabled,
                        isError,
                        interactionSource,
                        colors,
                        shape
                    )
                }
            )
        }
    )
}
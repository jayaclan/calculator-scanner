package com.calculator.scanner.ui.components.dropdown

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ExposedDropdownMenuBox
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import com.calculator.scanner.R
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.BVTTypography
import com.calculator.scanner.ui.theme.BlueOnesan
import com.calculator.scanner.ui.theme.CharacterDisabled
import com.calculator.scanner.ui.theme.Neutral10

@Composable
@Preview(showBackground = true)
private fun PreviewBVTDropDown() {
    var selectedPosProvinsi by remember { mutableStateOf(0) }
    val options = listOf("Jakarta", "Banten", "Jawa Barat", "Jawa Timur", "Jawa Tengah")
    BVTDropDown(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 4.dp),
        selectedPosition = selectedPosProvinsi,
        options = options,
        onStatusChange = {
            selectedPosProvinsi = it
            //Get ScheduleEvent
        }
    )
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun BVTDropDown(
    modifier: Modifier,
    selectedPosition: Int = 0,
    onStatusChange: (Int) -> Unit = { },
    options: List<String>
) {
    var expanded by remember { mutableStateOf(false) }
    var selectedOptionText by remember {
        mutableStateOf(options[selectedPosition])
    }

    var fieldSize by remember { mutableStateOf(Size.Zero) }

    ExposedDropdownMenuBox(
        expanded = expanded,
        onExpandedChange = {
            expanded = !expanded
        },
        modifier = modifier
    ) {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .onGloballyPositioned { coordinates ->
                    //This value is used to assign to the DropDown the same width
                    fieldSize = coordinates.size.toSize()
                },
            shape = RoundedCornerShape(4.dp)
        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                BVTText(
                    style = BVTTypography.H5Regular,
                    text = selectedOptionText,
                    color = CharacterDisabled.copy(0.85f),
                    modifier = Modifier
                        .weight(1f)
                        .padding(vertical = 8.dp, horizontal = 12.dp)
                )

                Image(
                    painter = painterResource(id = R.drawable.ic_arrow_down),
                    contentDescription = "",
                    modifier = Modifier
                        .padding(end = 16.dp)
                        .align(Alignment.CenterVertically)
                        .rotate(if (expanded) 180f else 360f)
                )
            }
        }

        ExposedDropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .width(with(LocalDensity.current) { fieldSize.width.toDp() })
                .background(Color.White)
        ) {
            options.forEachIndexed { index, selectionOption ->
                DropdownMenuItem(
                    modifier = Modifier.fillMaxWidth(),
                    onClick = {
                        onStatusChange(index)
                        selectedOptionText = selectionOption
                        expanded = false
                    },
                ) {
                    val color = if (selectedPosition == index) BlueOnesan else Neutral10
                    BVTText(
                        style = BVTTypography.H5Regular,
                        text = selectionOption,
                        color = color,
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(Color.White)
                    )
                }
            }
        }
    }
}
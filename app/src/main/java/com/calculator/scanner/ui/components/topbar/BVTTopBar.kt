package com.calculator.scanner.ui.components.topbar


import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.calculator.scanner.ui.components.text.BVTText
import com.calculator.scanner.ui.theme.BVTTypography
import com.calculator.scanner.ui.theme.BlueOnesan
import com.calculator.scanner.ui.theme.Primary1
import com.calculator.scanner.R
@Composable
@Preview(showBackground = true)
private fun PreviewBVTTopBar() {
    BVTTopBar(
        titleText = "Detail Testimony"
    )
}

@Composable
fun BVTTopBar(
    modifier: Modifier = Modifier,
    titleText: String = "",
    showTextBack: Boolean = true,
    background: Color = BlueOnesan,
    onClickBack: () -> Unit = {},
) {
    Box(
        modifier
            .fillMaxWidth()
            .background(background)
            .statusBarsPadding(),
        contentAlignment = Alignment.CenterStart
    ) {
        if (showTextBack) {
            TextButton(
                modifier = Modifier,
                onClick = onClickBack
            ) {
                Icon(
                    modifier = Modifier.size(12.dp),
                    painter = painterResource(id = R.drawable.ic_left),
                    tint = Primary1,
                    contentDescription = null
                )
//                BVTText(
//                    modifier = Modifier.padding(start = 6.dp),
//                    text = "Kembali", color = Primary1, style = BVTTypography.H5Regular
//                )
            }
        }

        BVTText(
            modifier = Modifier
                .padding(vertical = 16.dp)
                .fillMaxWidth(),
            text = titleText,
            color = Primary1,
            style = BVTTypography.H4Medium,
            textAlign = TextAlign.Center
        )
    }
}
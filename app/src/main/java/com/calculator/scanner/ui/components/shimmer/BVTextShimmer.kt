package com.calculator.scanner.ui.components.shimmer

import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp


@Composable
@Preview(showBackground = true)
fun BVTextShimmerPreview() {
    Column(Modifier.fillMaxWidth()) {
        BVTextShimmer(
            brush = Brush.linearGradient(
                listOf(
                    Color.LightGray.copy(alpha = 0.6f),
                    Color.LightGray.copy(alpha = 0.2f),
                    Color.LightGray.copy(alpha = 0.6f),
                )
            )
        )

        BVTextShimmer(
            brush = Brush.linearGradient(
                listOf(
                    Color.LightGray.copy(alpha = 0.6f),
                    Color.LightGray.copy(alpha = 0.2f),
                    Color.LightGray.copy(alpha = 0.6f),
                )
            ),
            index = 3
        )
    }

}

@Composable
fun BVTextShimmerAnimated(
    index: Int = 1,
    alignment: Alignment.Horizontal = CenterHorizontally,
    height: Dp = CustomHeight.H5,
    fraction: Float = 0.7f,
    spacer: Dp = 4.dp,
    paddingHorizontal: Dp = 16.dp
) {
    val shimmerColors = listOf(
        Color.LightGray.copy(alpha = 0.6f),
        Color.LightGray.copy(alpha = 0.2f),
        Color.LightGray.copy(alpha = 0.6f),
    )

    val transition = rememberInfiniteTransition()
    val translateAnim = transition.animateFloat(
        initialValue = 0f,
        targetValue = 1000f,
        animationSpec = infiniteRepeatable(
            animation = tween(
                durationMillis = 1000,
                easing = FastOutSlowInEasing
            ),
            repeatMode = RepeatMode.Reverse
        )
    )

    val brush = Brush.linearGradient(
        colors = shimmerColors,
        start = Offset.Zero,
        end = Offset(x = translateAnim.value, y = translateAnim.value)
    )

    BVTextShimmer(
        brush = brush,
        index = index,
        alignment = alignment,
        height = height,
        fraction = fraction,
        spacer = spacer,
        paddingHorizontal = paddingHorizontal
    )
}

@Composable
fun BVTextShimmer(
    brush: Brush,
    index: Int = 1,
    alignment: Alignment.Horizontal = CenterHorizontally,
    height: Dp = CustomHeight.H5,
    fraction: Float = 0.7f,
    spacer: Dp = 4.dp,
    paddingHorizontal: Dp = 16.dp
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        Spacer(modifier = Modifier.padding(4.dp))
        repeat(index) {
            Spacer(
                modifier = Modifier
                    .height(height)
                    .padding(horizontal = paddingHorizontal)
                    .clip(RoundedCornerShape(10.dp))
                    .fillMaxWidth(fraction = fraction)
                    .background(brush)
                    .align(alignment)
            )
            Spacer(modifier = Modifier.padding(spacer))
        }

    }
}

object CustomHeight {
    val H5: Dp = 20.dp
    val H4: Dp = 30.dp
    val H3: Dp = 40.dp
    val H2: Dp = 50.dp
}


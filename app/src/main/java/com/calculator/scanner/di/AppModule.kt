package com.calculator.scanner.di

import android.app.Application
import com.calculator.scanner.IndonesiaApp
import com.calculator.domain.di.DomainModule
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module(includes = [DomainModule::class])
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    @Named("baseUrl")
    fun provideBaseUrl(
        application: Application,
    ): String = (application as IndonesiaApp).getBaseUrl()

}
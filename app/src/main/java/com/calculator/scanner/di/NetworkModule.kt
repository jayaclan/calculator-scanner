package com.calculator.scanner.di

import com.calculator.base.network.createOkHttpClient
import com.calculator.data.datasource.local.UserPrefDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import javax.inject.Named

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun provideOkHttpClient(
        @Named("baseUrl") baseUrl: String,
        sharedPref: UserPrefDataSource
    ): OkHttpClient {
        return createOkHttpClient(
            baseUrl = baseUrl,
            getEmail = { sharedPref.userEmail },
            getToken = { sharedPref.userToken },
            onTokenUpdated = { sharedPref.userToken = it }
        )
    }

}
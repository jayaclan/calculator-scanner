#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_mvvmjetpackcompose_IndonesiaApp_baseUrl(
        JNIEnv *env,
        jobject
) {
//    std::string baseUrl = "http://api-kemendikbud.lokasi.dev/"; /* staging */
    std::string baseUrl = "http://api-kemendikbud.lokasi.dev/"; /* dev */
    return env->NewStringUTF(baseUrl.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_asuransibaik_mvvmjetpackcompose_IndonesiaApp_baseUrl(JNIEnv *env, jobject thiz) {

}
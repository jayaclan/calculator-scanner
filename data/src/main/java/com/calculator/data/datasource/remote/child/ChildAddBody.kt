package com.bvarta.data.datasource.remote.child

import com.google.gson.annotations.SerializedName


data class ChildAddBody(
    @SerializedName("jenis_kelamin")
    val jenisKelamin: String,
    @SerializedName("nama")
    val nama: String,
    @SerializedName("tgl_lahir")
    val tglLahir: String
)
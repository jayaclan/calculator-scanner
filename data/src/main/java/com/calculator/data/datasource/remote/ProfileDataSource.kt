package com.calculator.data.datasource.remote

import com.bvarta.data.datasource.remote.changepass.ChangePassBody
import com.bvarta.data.datasource.remote.changepass.ChangePassResponse
import com.bvarta.data.datasource.remote.channel.ChannelResponse
import com.bvarta.data.datasource.remote.channel.PostChannelBody
import com.bvarta.data.datasource.remote.channel.PostChannelResponse
import com.bvarta.data.datasource.remote.child.ChildAddBody
import com.bvarta.data.datasource.remote.child.ChildResponse
import com.bvarta.data.datasource.remote.child.ChildUpdateBody
import com.bvarta.data.datasource.remote.profession.ProfessionResponse
import com.bvarta.data.datasource.remote.selfdetail.SelfDetailResponse
import com.bvarta.data.datasource.remote.updateselfdetail.UpdateSelfDetailBody
import com.bvarta.data.datasource.remote.updateselfdetail.UpdateSelfDetailProfessionBody
import com.bvarta.data.datasource.remote.updateselfdetail.UpdateSelfDetailResponse
import com.skydoves.sandwich.ApiResponse
import okhttp3.MultipartBody
import retrofit2.http.*

interface ProfileDataSource {

    @PATCH("auth/users/update-password")
    suspend fun changePassword(
        @Body body: ChangePassBody
    ): ApiResponse<ChangePassResponse>

    @GET("event/api/kemendikbud/v1.0/biodata/get-detail")
    suspend fun getSelfDetail(): ApiResponse<SelfDetailResponse>

    @GET("event/api/kemendikbud/v1.0/biodata/get-detail")
    suspend fun getSelfDetail(
        @Header("Authorization") token: String
    ): ApiResponse<SelfDetailResponse>

    @POST("event/api/kemendikbud/v1.0/biodata/create-anak")
    suspend fun addChild(
        @Body body: ChildAddBody
    ): ApiResponse<ChildResponse>

    @PATCH("event/api/kemendikbud/v1.0/biodata/update/child")
    suspend fun updateChild(
        @Body body: ChildUpdateBody
    ): ApiResponse<ChildResponse>

    @PATCH("event/api/kemendikbud/v1.0/biodata/delete-anak/{id}")
    suspend fun deleteChild(
        @Path("id") id: String
    ): ApiResponse<ChildResponse>

    @PATCH("event/api/kemendikbud/v1.0/biodata/update/parent")
    suspend fun updateSelfDetail(
        @Body body: UpdateSelfDetailBody
    ): ApiResponse<UpdateSelfDetailResponse>

    @PATCH("event/api/kemendikbud/v1.0/biodata/update/parent")
    suspend fun updateSelfProfession(
        @Body body: UpdateSelfDetailProfessionBody
    ): ApiResponse<UpdateSelfDetailResponse>

    @Headers("multipart: true") // for stopping logging raw file
    @Multipart
    @POST("image/api/kemendikbud/v1.0/upload-profile")
    suspend fun updatePhotoDetail(
        @Part image: MultipartBody.Part,
    ): ApiResponse<UpdateSelfDetailResponse>

    @GET("event/api/kemendikbud/v1.0/pekerjaan/get-all")
    suspend fun getAllProfession(): ApiResponse<ProfessionResponse>

    @GET("event/api/kemendikbud/v1.0/discovery/get-all")
    suspend fun getAllChannel(): ApiResponse<ChannelResponse>

    @POST("event/api/kemendikbud/v1.0/discovery/create-summary")
    suspend fun postChannel(
        @Body body: PostChannelBody
    ): ApiResponse<PostChannelResponse>


}
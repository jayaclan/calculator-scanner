package com.calculator.data.datasource.local

import android.app.Application

interface UserPrefDataSource {
    fun appInit(application: Application)

    fun clear()

    fun saveSession(session: UserPrefDataSourceImpl.Session)

    fun getSession(): UserPrefDataSourceImpl.Session

    fun clearSession()

    var userToken: String?

    var userEmail: String?

}
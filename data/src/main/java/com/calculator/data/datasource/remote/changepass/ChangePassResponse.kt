package com.bvarta.data.datasource.remote.changepass

import android.os.Parcelable
import com.calculator.base.network.BaseRequest
import com.calculator.base.network.BaseResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChangePassResponse(

    override val message: String,

    override val request: BaseRequest,

    override val statusCode: Int,

    override val data: ChangePassData

) : BaseResponse<ChangePassData>(), Parcelable
package com.bvarta.data.datasource.remote.profession


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Profession(
    @SerializedName("id")
    val id: Int,
    @SerializedName("nama")
    val nama: String
) : Parcelable
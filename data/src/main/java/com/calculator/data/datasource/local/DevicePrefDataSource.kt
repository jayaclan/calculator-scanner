package com.calculator.data.datasource.local

import android.app.Application

interface DevicePrefDataSource {
    fun appInit(application: Application)

    fun clear()

    fun updateSession(session: DevicePrefDataSourceImpl.DeviceSession)

    fun getDeviceSession(): DevicePrefDataSourceImpl.DeviceSession

    var deviceOnboardPassed: Boolean

}
package com.bvarta.data.datasource.remote.selfdetail


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserProfile(
    @SerializedName("alamat")
    val alamat: String,
    @SerializedName("anak")
    val anak: List<UserProfile>?,
    @SerializedName("created_at")
    val createdAt: Long,
    @SerializedName("deleted_at")
    val deletedAt: Long,
    @SerializedName("email")
    val email: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("id_kabupaten_domisili")
    val idKabupatenDomisili: Int,
    @SerializedName("id_negara")
    val idNegara: Int,
    @SerializedName("id_parent")
    val idParent: Int,
    @SerializedName("id_provinsi_domisili")
    val idProvinsiDomisili: Int,
    @SerializedName("id_users")
    val idUsers: Int,
    @SerializedName("image_url")
    val imageUrl: String,
    @SerializedName("jenis_kelamin")
    val jenisKelamin: String,
    @SerializedName("kewarganegaraan")
    val kewarganegaraan: String,
    @SerializedName("nama")
    val nama: String,
    @SerializedName("nik")
    val nik: String?,
    @SerializedName("no_hp")
    val noHp: String,
    @SerializedName("passport")
    val passport: String?,
    @SerializedName("pekerjaan")
    val pekerjaan: Int?,
    @SerializedName("sim")
    val sim: String?,
    @SerializedName("tgl_lahir")
    val tglLahir: String,
    @SerializedName("updated_at")
    val updatedAt: Long,
) : Parcelable
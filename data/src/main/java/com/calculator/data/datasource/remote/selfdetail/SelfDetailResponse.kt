package com.bvarta.data.datasource.remote.selfdetail

import android.os.Parcelable
import com.calculator.base.network.BaseRequest
import com.calculator.base.network.BaseResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class SelfDetailResponse(

    override val message: String,

    override val request: BaseRequest,

    override val statusCode: Int,

    override val data: UserProfile

) : BaseResponse<UserProfile>(), Parcelable
package com.calculator.data.datasource.local

import android.app.Application
import com.orhanobut.hawk.Hawk

object DevicePrefDataSourceImpl : DevicePrefDataSource {
    private const val DEVICE_ONBOARD_PASSED = "deviceOnboardPassed"

    override fun appInit(application: Application) {
        Hawk.init(application).build()
    }

    override var deviceOnboardPassed: Boolean = false
        get() = Hawk.get(DEVICE_ONBOARD_PASSED) ?: false
        set(value) {
            Hawk.put(DEVICE_ONBOARD_PASSED, value)
            field = value
        }

    override fun clear() {
        Hawk.deleteAll()
    }

    override fun updateSession(session: DeviceSession) {
        deviceOnboardPassed = session.deviceOnboardPassed
    }

    override fun getDeviceSession() = DeviceSession(
        deviceOnboardPassed = deviceOnboardPassed
    )

    data class DeviceSession(
        val deviceOnboardPassed: Boolean,
    )
}
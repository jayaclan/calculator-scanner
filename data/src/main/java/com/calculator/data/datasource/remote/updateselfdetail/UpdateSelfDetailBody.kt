package com.bvarta.data.datasource.remote.updateselfdetail

import com.google.gson.annotations.SerializedName


data class UpdateSelfDetailBody(
    @SerializedName("nama")
    val nama: String,
    @SerializedName("alamat")
    val alamat: String,
    @SerializedName("id_negara")
    val id_negara: Int,
)
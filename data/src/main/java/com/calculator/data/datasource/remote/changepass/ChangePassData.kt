package com.bvarta.data.datasource.remote.changepass

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ChangePassData(
    @SerializedName("null")
    val emailUsername: String,
) : Parcelable
package com.bvarta.data.datasource.remote.changepass

import com.google.gson.annotations.SerializedName


data class ChangePassBody(
    @SerializedName("password")
    val password: String,
    @SerializedName("newPassword")
    val newPassword: String,
    @SerializedName("confirmNewPassword")
    val confirmNewPassword: String,
)
package com.bvarta.data.datasource.remote.updateselfdetail

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class UpdateSelfData(
    @SerializedName("file")
    val file: String
) : Parcelable

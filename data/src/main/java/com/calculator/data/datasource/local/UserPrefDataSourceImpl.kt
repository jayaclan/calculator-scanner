package com.calculator.data.datasource.local

import android.app.Application
import com.orhanobut.hawk.Hawk

object UserPrefDataSourceImpl : UserPrefDataSource {
    private const val USER_TOKEN = "userToken"
    private const val USER_REFRESH_TOKEN = "userRefreshToken"
    private const val USER_TOKEN_IAT = "userTokenIat"
    private const val USER_TOKEN_EXP = "userTokenExp"
    private const val USER_PHONE = "userPhone"
    private const val USER_FULL_NAME = "userFullName"
    private const val USER_FIRST_NAME = "userFirstName"
    private const val USER_LAST_NAME = "userLastName"
    private const val USER_EMAIL = "userEmail"
    private const val USER_ID = "userId"
    private const val USER_ID_PROFESSION = "userIdProfession"
    private const val USER_ID_PROVINCE = "userIdProvince"
    private const val USER_ID_CITY = "userIdCity"
    private const val USER_GENDER = "userGender"
    private const val USER_BIRTH_DATE = "userBirthDate"
    private const val USER_NIK = "userNIK"
    private const val USER_SIM = "userSIM"
    private const val USER_PASSPORT = "userPassport"
    private const val USER_NATIONALITY = "userNationality"
    private const val USER_ADDRESS = "userAddress"
    private const val USER_IMAGE = "userImage"

    override fun appInit(application: Application) {
        Hawk.init(application).build()
    }

    override var userToken: String? = null
        get() = Hawk.get(USER_TOKEN)
        set(value) {
            Hawk.put(USER_TOKEN, value)
            field = value
        }

    override var userEmail: String? = null
        get() = Hawk.get(USER_EMAIL)
        set(value) {
            Hawk.put(USER_EMAIL, value)
            field = value
        }

    private var userRefreshToken: String? = null
        get() = Hawk.get(USER_REFRESH_TOKEN)
        set(value) {
            Hawk.put(USER_REFRESH_TOKEN, value)
            field = value
        }
    private var userTokenIat: Long? = null
        get() = Hawk.get(USER_TOKEN_IAT)
        set(value) {
            Hawk.put(USER_TOKEN_IAT, value)
            field = value
        }
    private var userTokenExp: Long? = null
        get() = Hawk.get(USER_TOKEN_EXP)
        set(value) {
            Hawk.put(USER_TOKEN_EXP, value)
            field = value
        }
    private var userPhone: String? = null
        get() = Hawk.get(USER_PHONE)
        set(value) {
            Hawk.put(USER_PHONE, value)
            field = value
        }
    private var userFullName: String? = null
        get() = Hawk.get(USER_FULL_NAME)
        set(value) {
            Hawk.put(USER_FULL_NAME, value)
            field = value
        }
    private var userFirstName: String? = null
        get() = Hawk.get(USER_FIRST_NAME)
        set(value) {
            Hawk.put(USER_FIRST_NAME, value)
            field = value
        }
    private var userLastName: String? = null
        get() = Hawk.get(USER_LAST_NAME)
        set(value) {
            Hawk.put(USER_LAST_NAME, value)
            field = value
        }
    private var userId: Int? = null
        get() = Hawk.get(USER_ID)
        set(value) {
            Hawk.put(USER_ID, value)
            field = value
        }
    private var userIdProfession: Int? = null
        get() = Hawk.get(USER_ID_PROFESSION)
        set(value) {
            Hawk.put(USER_ID_PROFESSION, value)
            field = value
        }
    private var userIdProvince: Int? = null
        get() = Hawk.get(USER_ID_PROVINCE)
        set(value) {
            Hawk.put(USER_ID_PROVINCE, value)
            field = value
        }
    private var userIdCity: Int? = null
        get() = Hawk.get(USER_ID_CITY)
        set(value) {
            Hawk.put(USER_ID_CITY, value)
            field = value
        }
    private var userGender: String? = null
        get() = Hawk.get(USER_GENDER)
        set(value) {
            Hawk.put(USER_GENDER, value)
            field = value
        }
    private var userBirthDate: String? = null
        get() = Hawk.get(USER_BIRTH_DATE)
        set(value) {
            Hawk.put(USER_BIRTH_DATE, value)
            field = value
        }
    private var userNIK: String? = null
        get() = Hawk.get(USER_NIK)
        set(value) {
            Hawk.put(USER_NIK, value)
            field = value
        }
    private var userSIM: String? = null
        get() = Hawk.get(USER_SIM)
        set(value) {
            Hawk.put(USER_SIM, value)
            field = value
        }
    private var userPassport: String? = null
        get() = Hawk.get(USER_PASSPORT)
        set(value) {
            Hawk.put(USER_PASSPORT, value)
            field = value
        }
    private var userNationality: String? = null
        get() = Hawk.get(USER_NATIONALITY)
        set(value) {
            Hawk.put(USER_NATIONALITY, value)
            field = value
        }
    private var userAddress: String? = null
        get() = Hawk.get(USER_ADDRESS)
        set(value) {
            Hawk.put(USER_ADDRESS, value)
            field = value
        }
    private var userImage: String? = null
        get() = Hawk.get(USER_IMAGE)
        set(value) {
            Hawk.put(USER_IMAGE, value)
            field = value
        }

    override fun clear() {
        Hawk.deleteAll()
    }

    override fun saveSession(session: Session) {
        userToken = session.userToken
        userRefreshToken = session.userRefreshToken
        userTokenIat = session.userTokenIat
        userTokenExp = session.userTokenExp
        userPhone = session.userPhone
        userFullName = session.userFullName
        userFirstName = session.userFirstName
        userLastName = session.userLastName
        userEmail = session.userEmail
        userId = session.userId
        userIdProfession = session.userIdProfession
        userIdProvince = session.userIdProvince
        userIdCity = session.userIdCity
        userGender = session.userGender
        userBirthDate = session.userBirthDate
        userNIK = session.userNIK
        userSIM = session.userSIM
        userPassport = session.userPassport
        userNationality = session.userNationality
        userAddress = session.userAddress
        userImage = session.userImage
    }

    override fun getSession() = Session(
        userToken = userToken,
        userRefreshToken = userRefreshToken,
        userTokenIat = userTokenIat,
        userTokenExp = userTokenExp,
        userPhone = userPhone,
        userFullName = userFullName,
        userFirstName = userFirstName,
        userLastName = userLastName,
        userEmail = userEmail,
        userId = userId,
        userIdProfession = userIdProfession,
        userIdProvince = userIdProvince,
        userIdCity = userIdCity,
        userGender = userGender,
        userBirthDate = userBirthDate,
        userNIK = userNIK,
        userSIM = userSIM,
        userPassport = userPassport,
        userNationality = userNationality,
        userAddress = userAddress,
        userImage = userImage
    )

    override fun clearSession() {
        userToken = null
        userRefreshToken = null
        userTokenIat = null
        userTokenExp = null
        userPhone = null
        userFirstName = null
        userLastName = null
        userEmail = null
        userId = null
        userIdProfession = null
        userIdProvince = null
        userIdCity = null
        userGender = null
        userBirthDate = null
        userNIK = null
        userSIM = null
        userPassport = null
        userNationality = null
        userAddress = null
        userImage = null
    }

    data class Session(
        val userToken: String?,
        val userRefreshToken: String?,
        val userTokenIat: Long?,
        val userTokenExp: Long?,
        val userPhone: String?,
        val userFullName: String?,
        val userFirstName: String?,
        val userLastName: String?,
        val userEmail: String?,
        val userId: Int?,
        val userIdProfession: Int?,
        val userIdProvince: Int?,
        val userIdCity: Int?,
        val userGender: String?,
        val userBirthDate: String?,
        val userNIK: String?,
        val userSIM: String?,
        val userPassport: String?,
        val userNationality: String?,
        val userAddress: String?,
        val userImage: String?,
    )
}
package com.bvarta.data.datasource.remote.updateselfdetail

import android.os.Parcelable
import com.calculator.base.network.BaseRequest
import com.calculator.base.network.BaseResponse
import kotlinx.parcelize.Parcelize

@Parcelize
class UpdateSelfDetailResponse(
    override val message: String,

    override val request: BaseRequest,

    override val statusCode: Int,

    override val data: UpdateSelfData
) : BaseResponse<UpdateSelfData>(), Parcelable
package com.bvarta.data.datasource.remote.channel

import android.os.Parcelable
import com.calculator.base.network.BaseRequest
import com.calculator.base.network.BaseResponse
import kotlinx.parcelize.Parcelize

@Parcelize
data class PostChannelResponse(

    override val message: String,

    override val request: BaseRequest,

    override val statusCode: Int,

    override val data: String?

) : BaseResponse<String?>(), Parcelable
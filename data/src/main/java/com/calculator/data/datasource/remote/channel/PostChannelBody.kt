package com.bvarta.data.datasource.remote.channel


import com.google.gson.annotations.SerializedName

data class PostChannelBody(
    @SerializedName("discovery_channel")
    val discoveryChannel: List<DiscoveryChannel>
)
package com.bvarta.data.datasource.remote.channel


import com.google.gson.annotations.SerializedName

data class DiscoveryChannel(
    @SerializedName("id")
    val id: Int
)
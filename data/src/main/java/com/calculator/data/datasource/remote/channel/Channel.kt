package com.bvarta.data.datasource.remote.channel


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Channel(
    @SerializedName("id")
    val id: Int,
    @SerializedName("nama")
    val nama: String
) : Parcelable
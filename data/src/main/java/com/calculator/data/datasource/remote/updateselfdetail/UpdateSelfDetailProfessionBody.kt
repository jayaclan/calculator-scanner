package com.bvarta.data.datasource.remote.updateselfdetail

import com.google.gson.annotations.SerializedName


data class UpdateSelfDetailProfessionBody(
    @SerializedName("pekerjaan")
    val idProfession: Int
)
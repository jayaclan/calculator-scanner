package com.bvarta.data.datasource.remote.child

import com.google.gson.annotations.SerializedName


data class ChildUpdateBody(
    @SerializedName("id")
    val id: String,
    @SerializedName("nama")
    val nama: String,
    @SerializedName("tgl_lahir")
    val tglLahir: String
)
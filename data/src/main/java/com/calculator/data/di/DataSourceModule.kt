package com.calculator.data.di

import android.app.Application
import com.calculator.base.network.createApi
import com.calculator.data.datasource.local.DevicePrefDataSource
import com.calculator.data.datasource.local.DevicePrefDataSourceImpl
import com.calculator.data.datasource.local.UserPrefDataSource
import com.calculator.data.datasource.local.UserPrefDataSourceImpl
import com.calculator.data.datasource.remote.ProfileDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataSourceModule {

    @Provides
    @Singleton
    fun provideUserPrefDataSource(
        application: Application,
    ): UserPrefDataSource {
        val instance = UserPrefDataSourceImpl
        instance.appInit(application)
        return instance
    }

    @Provides
    @Singleton
    fun provideDevicePrefDataSource(
        application: Application,
    ): DevicePrefDataSource {
        val instance = DevicePrefDataSourceImpl
        instance.appInit(application)
        return instance
    }

    @Provides
    fun provideProfileDataSource(
        @Named("baseUrl") baseUrl: String,
        okHttpClient: OkHttpClient
    ): ProfileDataSource = createApi(okHttpClient, baseUrl)


}
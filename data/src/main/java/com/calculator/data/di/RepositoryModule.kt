package com.calculator.data.di

import com.calculator.data.datasource.local.DevicePrefDataSource
import com.calculator.data.datasource.local.UserPrefDataSource
import com.calculator.data.datasource.remote.ProfileDataSource
import com.calculator.data.repository.ProfileRepository
import com.calculator.data.repository.ProfileRepositoryImpl
import com.calculator.data.repository.SessionRepository
import com.calculator.data.repository.SessionRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {


    @Provides
    fun provideProfileRepository(
        userPrefDataSource: UserPrefDataSource,
        profileDataSource: ProfileDataSource,
        coroutineDispatcher: CoroutineDispatcher
    ): ProfileRepository = ProfileRepositoryImpl(
        userPrefDataSource,
        profileDataSource,
        coroutineDispatcher
    )

    @Provides
    fun provideSessionRepository(
        userPrefDataSource: UserPrefDataSource,
        devicePrefDataSource: DevicePrefDataSource,
        coroutineDispatcher: CoroutineDispatcher
    ): SessionRepository = SessionRepositoryImpl(
        userPrefDataSource,
        devicePrefDataSource,
        coroutineDispatcher
    )


}
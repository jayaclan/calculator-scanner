package com.calculator.data.repository

import com.calculator.base.repository.BaseRepository
import com.calculator.data.datasource.local.DevicePrefDataSource
import com.calculator.data.datasource.local.DevicePrefDataSourceImpl
import com.calculator.data.datasource.local.UserPrefDataSource
import com.calculator.data.datasource.local.UserPrefDataSourceImpl
import kotlinx.coroutines.CoroutineDispatcher

class SessionRepositoryImpl(
    private val userPrefDataSource: UserPrefDataSource,
    private val devicePrefDataSource: DevicePrefDataSource,
    ioDispatcher: CoroutineDispatcher
) : BaseRepository(ioDispatcher), SessionRepository {

    override fun saveUserSession(session: UserPrefDataSourceImpl.Session) {
        userPrefDataSource.saveSession(session)
    }

    override fun getUserSession() = userPrefDataSource.getSession()

    override fun clearUserSession() {
        userPrefDataSource.clearSession()
    }

    override fun saveDeviceSession(
        deviceOnboardPassed: Boolean
    ) {
        val session = DevicePrefDataSourceImpl.DeviceSession(
            deviceOnboardPassed = deviceOnboardPassed
        )
        devicePrefDataSource.updateSession(session)
    }

    override fun getDeviceSession() = devicePrefDataSource.getDeviceSession()
}
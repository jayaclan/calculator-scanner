package com.calculator.data.repository

import com.bvarta.data.datasource.remote.changepass.ChangePassResponse
import com.bvarta.data.datasource.remote.channel.ChannelResponse
import com.bvarta.data.datasource.remote.channel.PostChannelResponse
import com.bvarta.data.datasource.remote.child.ChildResponse
import com.bvarta.data.datasource.remote.profession.ProfessionResponse
import com.bvarta.data.datasource.remote.selfdetail.SelfDetailResponse
import com.bvarta.data.datasource.remote.updateselfdetail.UpdateSelfDetailResponse
import com.calculator.base.network.Resource
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody

interface ProfileRepository {
    suspend fun changePassword(
        password: String,
        newPassword: String,
        confirmNewPassword: String
    ): Flow<Resource<ChangePassResponse>>

    suspend fun getSelfDetail(): Flow<Resource<SelfDetailResponse>>

    suspend fun getSelfDetail(token: String?): Flow<Resource<SelfDetailResponse>>

    suspend fun addChild(name: String, gender: String, date: String): Flow<Resource<ChildResponse>>

    suspend fun updateChild(id: String, name: String, date: String): Flow<Resource<ChildResponse>>

    suspend fun deleteChild(id: String): Flow<Resource<ChildResponse>>

    suspend fun updatesSelfDetail(
        name: String,
        address: String,
        idCountry: Int,
    ): Flow<Resource<UpdateSelfDetailResponse>>

    suspend fun updatesSelfDetail(idProfession: Int): Flow<Resource<UpdateSelfDetailResponse>>

    suspend fun updatePhotoDetail(image: MultipartBody.Part): Flow<Resource<UpdateSelfDetailResponse>>

    suspend fun getAllProfession(): Flow<Resource<ProfessionResponse>>

    suspend fun getAllChannel(): Flow<Resource<ChannelResponse>>

    suspend fun postChannel(idChannels: List<Int>): Flow<Resource<PostChannelResponse>>
}
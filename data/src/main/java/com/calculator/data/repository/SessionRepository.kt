package com.calculator.data.repository

import com.calculator.data.datasource.local.DevicePrefDataSourceImpl
import com.calculator.data.datasource.local.UserPrefDataSourceImpl

interface SessionRepository {
    fun saveUserSession(session: UserPrefDataSourceImpl.Session)

    fun getUserSession(): UserPrefDataSourceImpl.Session

    fun clearUserSession()

    fun saveDeviceSession(deviceOnboardPassed: Boolean)

    fun getDeviceSession(): DevicePrefDataSourceImpl.DeviceSession
}
package com.calculator.data.repository

import com.bvarta.data.datasource.remote.changepass.ChangePassBody
import com.bvarta.data.datasource.remote.changepass.ChangePassResponse
import com.bvarta.data.datasource.remote.channel.ChannelResponse
import com.bvarta.data.datasource.remote.channel.DiscoveryChannel
import com.bvarta.data.datasource.remote.channel.PostChannelBody
import com.bvarta.data.datasource.remote.channel.PostChannelResponse
import com.bvarta.data.datasource.remote.child.ChildAddBody
import com.bvarta.data.datasource.remote.child.ChildResponse
import com.bvarta.data.datasource.remote.child.ChildUpdateBody
import com.bvarta.data.datasource.remote.profession.ProfessionResponse
import com.bvarta.data.datasource.remote.selfdetail.SelfDetailResponse
import com.bvarta.data.datasource.remote.updateselfdetail.UpdateSelfDetailBody
import com.bvarta.data.datasource.remote.updateselfdetail.UpdateSelfDetailProfessionBody
import com.bvarta.data.datasource.remote.updateselfdetail.UpdateSelfDetailResponse
import com.calculator.base.network.Resource
import com.calculator.base.repository.BaseRepository
import com.calculator.data.datasource.local.UserPrefDataSource
import com.calculator.data.datasource.remote.ProfileDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MultipartBody

class ProfileRepositoryImpl(
    private val userPrefDataSource: UserPrefDataSource,
    private val profileDataSource: ProfileDataSource,
    ioDispatcher: CoroutineDispatcher
) : BaseRepository(ioDispatcher), ProfileRepository {

    override suspend fun changePassword(
        password: String,
        newPassword: String,
        confirmNewPassword: String
    ): Flow<Resource<ChangePassResponse>> {
        val body = ChangePassBody(password, newPassword, confirmNewPassword)
        return fetchRemote(profileDataSource.changePassword(body))
    }

    override suspend fun getSelfDetail(): Flow<Resource<SelfDetailResponse>> = getSelfDetail(null)

    override suspend fun getSelfDetail(token: String?): Flow<Resource<SelfDetailResponse>> = flow {
        emit(Resource.Loading)
        val endpoint =
            if (token == null) profileDataSource.getSelfDetail()
            else profileDataSource.getSelfDetail("Bearer $token")
        fetchRemote(endpoint).collect {
            when (it) {
                is Resource.Loading -> emit(it)
                is Resource.Exception -> emit(it)
                is Resource.Success -> {
                    val data = it.value.data
                    val name = data.nama.split(" ")
                    val session = userPrefDataSource.getSession()
                    userPrefDataSource.saveSession(
                        session.copy(
                            userPhone = data.noHp,
                            userFullName = data.nama,
                            userFirstName = name.first(),
                            userLastName = name.last(),
                            userEmail = data.email
                        )
                    )
                    emit(it)
                }
            }
        }
    }

    override suspend fun addChild(
        name: String, gender: String, date: String
    ): Flow<Resource<ChildResponse>> {
        val body = ChildAddBody(nama = name, jenisKelamin = gender, tglLahir = date)
        return fetchRemote(profileDataSource.addChild(body))
    }

    override suspend fun updateChild(
        id: String,
        name: String,
        date: String
    ): Flow<Resource<ChildResponse>> {
        val body = ChildUpdateBody(id = id, nama = name, tglLahir = date)
        return fetchRemote(profileDataSource.updateChild(body))
    }

    override suspend fun deleteChild(id: String): Flow<Resource<ChildResponse>> {
        return fetchRemote(profileDataSource.deleteChild(id))
    }

    override suspend fun updatesSelfDetail(
        name: String,
        address: String,
        idCountry: Int,
    ): Flow<Resource<UpdateSelfDetailResponse>> {
        val body = UpdateSelfDetailBody(
            nama = name,
            alamat = address,
            id_negara = idCountry
        )
        return fetchRemote(profileDataSource.updateSelfDetail(body))
    }

    override suspend fun updatesSelfDetail(idProfession: Int): Flow<Resource<UpdateSelfDetailResponse>> {
        val body = UpdateSelfDetailProfessionBody(
            idProfession = idProfession,
        )
        return fetchRemote(profileDataSource.updateSelfProfession(body))
    }

    override suspend fun updatePhotoDetail(image: MultipartBody.Part): Flow<Resource<UpdateSelfDetailResponse>> {
        return fetchRemote(profileDataSource.updatePhotoDetail(image))
    }

    override suspend fun getAllProfession(): Flow<Resource<ProfessionResponse>> {
        return fetchRemote(profileDataSource.getAllProfession())
    }

    override suspend fun getAllChannel(): Flow<Resource<ChannelResponse>> {
        return fetchRemote(profileDataSource.getAllChannel())
    }

    override suspend fun postChannel(idChannels: List<Int>): Flow<Resource<PostChannelResponse>> {
        val body = PostChannelBody(idChannels.map {
            DiscoveryChannel(it)
        })
        return fetchRemote(profileDataSource.postChannel(body))
    }

}
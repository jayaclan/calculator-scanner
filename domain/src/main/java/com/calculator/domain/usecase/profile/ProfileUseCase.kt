package com.calculator.domain.usecase.profile

import com.calculator.base.network.Resource
import com.calculator.domain.model.IdNameEntity
import com.calculator.domain.model.UserEntity
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody

interface ProfileUseCase {

    suspend fun changePassword(
        password: String,
        newPassword: String,
        confirmNewPassword: String
    ): Flow<Resource<Boolean>>

    suspend fun getSelfDetail(): Flow<Resource<UserEntity>>

    suspend fun addChild(name: String, gender: String, date: String): Flow<Resource<Boolean>>

    suspend fun updateChild(id: String, name: String, date: String): Flow<Resource<Boolean>>

    suspend fun deleteChild(id: String): Flow<Resource<Boolean>>

    suspend fun updatesSelfDetail(
        name: String,
        address: String,
        idCountry: Int,
    ): Flow<Resource<Boolean>>

    suspend fun updatesSelfDetail(idProfession: Int): Flow<Resource<Boolean>>

    suspend fun updatePhotoDetail(image: MultipartBody.Part): Flow<Resource<String>>

    suspend fun getAllProfession(): Flow<Resource<List<IdNameEntity>>>

    suspend fun getAllChannel(): Flow<Resource<List<IdNameEntity>>>

    suspend fun postChannel(idChannels: List<Int>): Flow<Resource<Boolean>>

}
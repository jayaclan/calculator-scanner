package com.calculator.domain.usecase.profile

import com.bvarta.data.datasource.remote.selfdetail.UserProfile
import com.calculator.base.network.Resource
import com.calculator.data.repository.ProfileRepository
import com.calculator.domain.model.IdNameEntity
import com.calculator.domain.model.UserEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MultipartBody
import java.util.*
import javax.inject.Inject

class ProfileUseCaseImpl @Inject constructor(
    private val profileRepository: ProfileRepository
) : ProfileUseCase {

    override suspend fun changePassword(
        password: String,
        newPassword: String,
        confirmNewPassword: String
    ) = flow {
        profileRepository.changePassword(password, newPassword, confirmNewPassword).collect {
            when (it) {
                is Resource.Loading -> emit(it)
                is Resource.Exception -> emit(it)
                is Resource.Success -> emit(Resource.Success(true))
            }
        }
    }

    override suspend fun getSelfDetail(): Flow<Resource<UserEntity>> = flow {
        profileRepository.getSelfDetail().collect {
            when (it) {
                is Resource.Loading -> emit(it)
                is Resource.Exception -> emit(it)
                is Resource.Success -> {
                    emit(Resource.Success(it.value.data.toUserEntity()))
                }
            }
        }
    }

    override suspend fun addChild(name: String, gender: String, date: String) = flow {
        profileRepository.addChild(name, gender, date).collect {
            when (it) {
                is Resource.Loading -> emit(it)
                is Resource.Exception -> emit(it)
                is Resource.Success -> emit(Resource.Success(true))
            }
        }
    }

    override suspend fun updateChild(
        id: String,
        name: String,
        date: String
    ) = flow {
        profileRepository.updateChild(id, name, date).collect {
            when (it) {
                is Resource.Loading -> emit(it)
                is Resource.Exception -> emit(it)
                is Resource.Success -> emit(Resource.Success(true))
            }
        }
    }

    override suspend fun deleteChild(id: String) = flow {
        profileRepository.deleteChild(id).collect {
            when (it) {
                is Resource.Loading -> emit(it)
                is Resource.Exception -> emit(it)
                is Resource.Success -> emit(Resource.Success(true))
            }
        }
    }

    override suspend fun updatesSelfDetail(
        name: String,
        address: String,
        idCountry: Int,
    ) = flow {
        profileRepository.updatesSelfDetail(
            name = name,
            address = address,
            idCountry = idCountry
        ).collect {
            when (it) {
                is Resource.Loading -> emit(it)
                is Resource.Exception -> emit(it)
                is Resource.Success -> emit(Resource.Success(true))
            }
        }
    }

    override suspend fun updatesSelfDetail(idProfession: Int): Flow<Resource<Boolean>> = flow {
        profileRepository.updatesSelfDetail(
            idProfession = idProfession
        ).collect {
            when (it) {
                is Resource.Loading -> emit(it)
                is Resource.Exception -> emit(it)
                is Resource.Success -> emit(Resource.Success(true))
            }
        }
    }

    override suspend fun updatePhotoDetail(image: MultipartBody.Part): Flow<Resource<String>> =
        flow {
            profileRepository.updatePhotoDetail(image).collect {
                when (it) {
                    is Resource.Loading -> emit(it)
                    is Resource.Exception -> emit(it)
                    is Resource.Success -> emit(Resource.Success(it.value.data.file))
                }
            }
        }

    override suspend fun getAllProfession(): Flow<Resource<List<IdNameEntity>>> = flow {
        profileRepository.getAllProfession().collect { res ->
            when (res) {
                is Resource.Loading -> emit(res)
                is Resource.Exception -> emit(res)
                is Resource.Success -> emit(Resource.Success(res.value.data.map {
                    IdNameEntity(id = it.id, name = it.nama)
                }))
            }
        }
    }

    override suspend fun getAllChannel(): Flow<Resource<List<IdNameEntity>>> = flow {
        profileRepository.getAllChannel().collect { res ->
            when (res) {
                is Resource.Loading -> emit(res)
                is Resource.Exception -> emit(res)
                is Resource.Success -> emit(Resource.Success(res.value.data.map {
                    IdNameEntity(id = it.id, name = it.nama)
                }))
            }
        }
    }

    override suspend fun postChannel(idChannels: List<Int>): Flow<Resource<Boolean>> = flow {
        profileRepository.postChannel(idChannels).collect { res ->
            when (res) {
                is Resource.Loading -> emit(res)
                is Resource.Exception -> emit(res)
                is Resource.Success -> emit(Resource.Success(true))
            }
        }
    }

    private fun UserProfile.toUserEntity(): UserEntity = UserEntity(
        address = alamat,
        birthDate = tglLahir,
        members = anak?.map { it.toUserEntity() } ?: listOf(),
        createdAt = Date(createdAt),
        email = email,
        fullName = nama,
        gender = jenisKelamin,
        id = id,
        idCity = idKabupatenDomisili,
        idCountry = idNegara,
        idParent = idParent,
        idProfession = pekerjaan,
        idProvince = idProvinsiDomisili,
        idUsers = idUsers,
        imageUrl = imageUrl,
        nationality = kewarganegaraan,
        nik = nik,
        phone = noHp,
        passport = passport,
        sim = sim
    )
}
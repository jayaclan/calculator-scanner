package com.calculator.domain.usecase.device

import com.calculator.base.network.Resource
import com.calculator.data.repository.SessionRepository
import com.calculator.domain.model.DeviceSessionEntity
import com.calculator.domain.model.UserSessionEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DeviceUseCaseImpl @Inject constructor(
    private val sessionRepository: SessionRepository
) : DeviceUseCase {
    override suspend fun updateSession(deviceSession: DeviceSessionEntity) {
        sessionRepository.saveDeviceSession(
            deviceOnboardPassed = deviceSession.deviceOnboardPassed,
        )
    }

    override suspend fun getDeviceSession(): Flow<Resource<DeviceSessionEntity>> = flow {
        val result = sessionRepository.getDeviceSession()
        emit(
            Resource.Success(
                DeviceSessionEntity(
                    deviceOnboardPassed = result.deviceOnboardPassed
                )
            )
        )
    }

    override suspend fun getSession(): Flow<Resource<UserSessionEntity>> = flow {
        val loginSession = sessionRepository.getUserSession()
        emit(
            Resource.Success(
                UserSessionEntity(
                    token = loginSession.userToken,
                    refreshToken = loginSession.userRefreshToken,
                    iat = loginSession.userTokenIat ?: -1,
                    exp = loginSession.userTokenExp ?: -1,
                    phone = loginSession.userPhone,
                    firstName = loginSession.userFirstName,
                    lastName = loginSession.userLastName,
                    email = loginSession.userEmail
                )
            )
        )
    }
}
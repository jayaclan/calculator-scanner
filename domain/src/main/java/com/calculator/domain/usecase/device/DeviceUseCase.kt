package com.calculator.domain.usecase.device

import com.calculator.base.network.Resource
import com.calculator.domain.model.DeviceSessionEntity
import com.calculator.domain.model.UserSessionEntity
import kotlinx.coroutines.flow.Flow

interface DeviceUseCase {

    suspend fun updateSession(deviceSession: DeviceSessionEntity)

    suspend fun getDeviceSession(): Flow<Resource<DeviceSessionEntity>>

    suspend fun getSession(): Flow<Resource<UserSessionEntity>>

}
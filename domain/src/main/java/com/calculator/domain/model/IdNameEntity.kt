package com.calculator.domain.model


import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class IdNameEntity(
    val id: Int,
    val name: String
) : Parcelable
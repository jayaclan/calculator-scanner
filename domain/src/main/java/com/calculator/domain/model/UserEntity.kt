package com.calculator.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class UserEntity(
    val address: String,
    val birthDate: String, // YYYY-MM-DD
    val members: List<UserEntity> = listOf(), // if json null, then insert empty array
    val createdAt: Date,
    val email: String?,
    val fullName: String,
    val gender: String,
    val id: Int,
    val idCity: Int?,
    val idCountry: Int?,
    val idParent: Int,
    val idProfession: Int?,
    val idProvince: Int?,
    val idUsers: Int?,
    val imageUrl: String?,
    val nationality: String?,
    val nik: String?,
    val phone: String?,
    val passport: String?,
    val sim: String?,
) : Parcelable


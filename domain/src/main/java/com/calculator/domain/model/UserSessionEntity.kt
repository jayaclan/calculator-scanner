package com.calculator.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserSessionEntity(
    val token: String? = null,
    val refreshToken: String? = null,
    val iat: Long = -1,
    val exp: Long = -1,
    val phone: String? = null,
    val firstName: String? = null,
    val lastName: String? = null,
    val email: String? = null,
    val alreadyLogin: Boolean = false
) : Parcelable

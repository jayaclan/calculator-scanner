package com.calculator.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DeviceSessionEntity(
    val deviceOnboardPassed: Boolean = false,
) : Parcelable

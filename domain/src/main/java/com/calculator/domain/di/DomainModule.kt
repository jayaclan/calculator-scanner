package com.calculator.domain.di

import com.calculator.data.di.DataSourceModule
import com.calculator.data.di.RepositoryModule
import com.calculator.data.repository.ProfileRepository
import com.calculator.data.repository.SessionRepository
import com.calculator.domain.usecase.device.DeviceUseCase
import com.calculator.domain.usecase.device.DeviceUseCaseImpl
import com.calculator.domain.usecase.profile.ProfileUseCase
import com.calculator.domain.usecase.profile.ProfileUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module(includes = [DataSourceModule::class, RepositoryModule::class])
@InstallIn(SingletonComponent::class)
object DomainModule {


    @Provides
    @Singleton
    fun provideProfileUseCase(
        profileRepository: ProfileRepository
    ): ProfileUseCase = ProfileUseCaseImpl(profileRepository)

    @Provides
    @Singleton
    fun provideDeviceUseCase(
        sessionRepository: SessionRepository
    ): DeviceUseCase = DeviceUseCaseImpl(sessionRepository)

}